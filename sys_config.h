/**
 * @file sys_config.h
 * @brief description
 *
 *  detail description
 *
 * @Author:  chao
 * @version: V1.0.0
 * @date:    May 28, 2020
 * @par Copyright(c)
 *
 *     SZU
 *
 * @par History
 *
 * Date				Author			Notes
 * May 28, 2020			chao  	create first version	
 **/

#ifndef SYS_CONFIG_H_
#define SYS_CONFIG_H_

#define RTTHREAD

#if defined(FREERTOS)
#include <FreeRTOSConfig.h>
#define THREAD_STACK_MIN_SIZE	configMINIMAL_STACK_SIZE
#define THREAD_PRIORITY_DEFAULT	configMAX_PRIORITIES/2

#define THREAD_NAME_ROBOTICS_COM_UART				"rob.com.uart"
#define THREAD_STACK_SIZE_ROBOTICS_COM_UART 		THREAD_STACK_MIN_SIZE
#define THREAD_PRIORITY_ROBOTICS_COM_UART			3

#elif defined(LINUX)
#define THREAD_STACK_MIN_SIZE	8*1024*1024
#define THREAD_PRIORITY_DEFAULT	50

#elif defined(RTTHREAD)
#include <rtthread.h>

#define SYS_THREAD_PRIORITY_MAX    			                (RT_THREAD_PRIORITY_MAX-1)
#define SYS_THREAD_STACK_MIN_SIZE	 		                256

#define SYS_THREAD_NAME_LED                                 "led"
#define SYS_THREAD_STACK_SIZE_LED                           SYS_THREAD_STACK_MIN_SIZE
#define SYS_THREAD_PRIORITY_LED                             3
#define SYS_THREAD_TIME_SLICE_LED                           10


#define SYS_THREAD_NAME_BTN                                 "btn"
#define SYS_THREAD_STACK_SIZE_BTN                           SYS_THREAD_STACK_MIN_SIZE
#define SYS_THREAD_PRIORITY_BTN                             (4)
#define SYS_THREAD_TIME_SLICE_BTN                           10

#define SYS_THREAD_NAME_NET_CLIENT                          "netclient"
#define SYS_THREAD_STACK_SIZE_NET_CLIENT                    4*SYS_THREAD_STACK_MIN_SIZE
#define SYS_THREAD_PRIORITY_NET_CLIENT                      (7)
#define SYS_THREAD_TIME_SLICE_NET_CLIENT                    20

#define SYS_THREAD_NAME_NET_CLIENT_PARSE                    "netparse"
#define SYS_THREAD_STACK_SIZE_NET_CLIENT_PARSE              2*SYS_THREAD_STACK_MIN_SIZE
#define SYS_THREAD_PRIORITY_NET_CLIENT_PARSE                (6)
#define SYS_THREAD_TIME_SLICE_NET_CLIENT_PARSE              20


//#define INTERRUPT_PRIORIITY_FO              0
//#define INTERRUPT_PRIORIITY_OC              1
//#define INTERRUPT_PRIORIITY_TIM_UPDATE      2
//#define INTERRUPT_PRIORIITY_TIM_CAPTURE     3
//#define INTERRUPT_PRIORIITY_ADC_DMA_TC      4
//#define INTERRUPT_PRIORIITY_TIM3_UPDATE     5


#endif



#endif /* SYS_CONFIG_H_ */
