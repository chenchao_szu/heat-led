/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-12-31     chao       the first version
 */
#include "stm32f1xx_hal.h"
#include "hal.h"
#include <string.h>

#include <rtthread.h>
#include <rthw.h>

#define PRIORITY_UART_DMA_RX        5

#define UART1_DMA_RXBUF_SIZE         255
#define LOG_CONSOLEBUF_SIZE          255
UART_HandleTypeDef huart1;
DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart1_tx;
uint8_t uart1_rx_buf[UART1_DMA_RXBUF_SIZE];
uint8_t uart1_rx_len;

data_in func_data_in;
void    *usrdata;


static void _error_handler()
{
    while(1);
}


static void _rs485_gpio_init()
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    RS485_EN_CLK_ENABLE();
    __HAL_RCC_AFIO_CLK_ENABLE();

    __HAL_AFIO_REMAP_SWJ_NOJTAG();

    /*Configure GPIO pin : PB5 */
    GPIO_InitStruct.Pin = RS485_EN_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(RS485_EN_PORT, &GPIO_InitStruct);
}


int hal_uart_init(uint32_t baud)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    func_data_in = NULL;
    usrdata = NULL;

    _rs485_gpio_init();

    __HAL_RCC_AFIO_CLK_ENABLE();


    /* USART1 DMA Init */
    /* DMA controller clock enable */
    __HAL_RCC_DMA1_CLK_ENABLE();


    huart1.Instance = USART1;
    huart1.Init.BaudRate = baud;
    huart1.Init.WordLength = UART_WORDLENGTH_8B;
    huart1.Init.StopBits = UART_STOPBITS_1;
    huart1.Init.Parity = UART_PARITY_NONE;
    huart1.Init.Mode = UART_MODE_TX_RX;
    huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart1.Init.OverSampling = UART_OVERSAMPLING_16;

    /* USER CODE END USART1_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_USART1_CLK_ENABLE();

    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**USART1 GPIO Configuration
    PB6     ------> USART1_TX
    PB7     ------> USART1_RX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_6;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    __HAL_AFIO_REMAP_USART1_ENABLE();

    /* USART1 DMA Init */
    /* USART1_RX Init */
    hdma_usart1_rx.Instance = DMA1_Channel5;
    hdma_usart1_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_usart1_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_usart1_rx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_usart1_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_usart1_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_usart1_rx.Init.Mode = DMA_NORMAL;
    hdma_usart1_rx.Init.Priority = DMA_PRIORITY_HIGH;
    if (HAL_DMA_Init(&hdma_usart1_rx) != HAL_OK)
    {
      _error_handler();
    }

    __HAL_LINKDMA(&huart1,hdmarx,hdma_usart1_rx);


    /* USART1 interrupt Init */
    HAL_NVIC_SetPriority(USART1_IRQn, PRIORITY_UART_DMA_RX, 0);
    HAL_NVIC_EnableIRQ(USART1_IRQn);

//    HAL_DMA_RegisterCallback(&hdma_usart1_tx, HAL_DMA_XFER_CPLT_CB_ID, on_uart_dma_transfer_complete);


    if (HAL_UART_Init(&huart1) != HAL_OK)
    {
        _error_handler();
    }

    __HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE); //使能IDLE中断

    //DMA接收函数，此句一定要加，不加接收不到第一次传进来的实数据，是空的，且此时接收到的数据长度为缓存器的数据长度
     HAL_UART_Receive_DMA(&huart1,uart1_rx_buf,UART1_DMA_RXBUF_SIZE);
//    HAL_UART_Receive_IT(&huart1, pData, Size)


    return 0;
}
void hal_uart_deinit()
{
    __HAL_RCC_USART1_CLK_DISABLE();

    /**USART1 GPIO Configuration
    PB6     ------> USART1_TX
    PB7     ------> USART1_RX
    */
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_6|GPIO_PIN_7);

    /* USART1 DMA DeInit */
    HAL_DMA_DeInit(huart1.hdmarx);

    /* USART1 interrupt DeInit */
    HAL_NVIC_DisableIRQ(USART1_IRQn);
}

int hal_uart_send(uint8_t *data,int len ,int timeout)
{
    if( HAL_OK != HAL_UART_Transmit(&huart1, data, len, timeout)) return -1;
    while(__HAL_UART_GET_FLAG(&huart1,UART_FLAG_TC)!=SET);
    return len;
}

void hal_uart_register_recv_data_callback(data_in func , void *usr_data)
{
    func_data_in = func;
    usrdata = usr_data;
}

int  hal_rs485_send(uint8_t *data,int len ,int timeout)
{
    int ret = -1;
    RS485_TX_EN();
    ret = hal_uart_send(data, len, timeout);
    RS485_RX_EN();
    return ret;
}

void hal_rs485_printf(const char *fmt, ...)
{
    va_list args;
    rt_size_t length;
    rt_size_t size = 0;

    static char rt_log_buf[LOG_CONSOLEBUF_SIZE];

    va_start(args, fmt);
    /* the return value of vsnprintf is the number of bytes that would be
     * written to buffer had if the size of the buffer been sufficiently
     * large excluding the terminating null byte. If the output string
     * would be larger than the rt_log_buf, we have to adjust the output
     * length. */
    length = rt_vsnprintf(rt_log_buf, sizeof(rt_log_buf) - 1, fmt, args);
    if (length > LOG_CONSOLEBUF_SIZE - 1)
        length = LOG_CONSOLEBUF_SIZE - 1;

    size = rt_strlen(rt_log_buf);

    hal_rs485_send((uint8_t*)rt_log_buf,size,1000);

    va_end(args);
}

void hal_rs485_dma_rx_enable()
{
    HAL_UART_Receive_DMA(&huart1,uart1_rx_buf,UART1_DMA_RXBUF_SIZE);
}
void hal_rs485_dma_rx_disable()
{
    HAL_UART_DMAStop(&huart1);
}

void USART1_IRQHandler(void)
{
    uint32_t temp;

    if(__HAL_UART_GET_FLAG(&huart1,UART_FLAG_IDLE)!= RESET)//如果接受到了一帧数据
    {
        __HAL_UART_CLEAR_IDLEFLAG(&huart1);//清除标志位
        //temp = huart1.Instance->SR;  //清除状态寄存器SR,读取SR寄存器可以实现清除SR寄存器的功能
        //temp = huart1.Instance->DR; //读取数据寄存器中的数据
        HAL_UART_DMAStop(&huart1); //
        temp  =  __HAL_DMA_GET_COUNTER(&hdma_usart1_rx);// 获取DMA中未传输的数据个数
        //temp  = hdma_usart1_rx.Instance->NDTR;//读取NDTR寄存器 获取DMA中未传输的数据个数，
        //这句和上面那句等效
        uart1_rx_len =  UART1_DMA_RXBUF_SIZE - temp; //总计数减去未传输的数据个数，得到已经接收的数据个数

        if(func_data_in != NULL)
        {
            func_data_in(uart1_rx_buf,uart1_rx_len,usrdata);
        }

        hal_rs485_dma_rx_enable();
     }

//    HAL_UART_IRQHandler(&huart1);
}



int  hal_led_init()
{
//    GPIO_InitTypeDef GPIO_InitStruct = {0};

//    /* GPIO Ports Clock Enable */
//    __HAL_RCC_GPIOA_CLK_ENABLE();
//
//    /*Configure GPIO pin Output Level */
//    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_RESET);
//
//    /*Configure GPIO pin : PA2 */
//    GPIO_InitStruct.Pin = GPIO_PIN_2 ;
//    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//    GPIO_InitStruct.Pull = GPIO_NOPULL;
//    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
//    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
//    return 0;

    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable */
    LED_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(LED_PORT, LED_PIN, GPIO_PIN_RESET);

    /*Configure GPIO pin : PA2 */
    GPIO_InitStruct.Pin = LED_PIN ;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(LED_PORT, &GPIO_InitStruct);
    return 0;
}
