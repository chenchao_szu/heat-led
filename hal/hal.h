/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-12-31     chao       the first version
 */
#ifndef HAL_H_
#define HAL_H_
#include <stm32f1xx.h>

#define RS485_EN_PIN            GPIO_PIN_15
#define RS485_EN_PORT           GPIOA
#define RS485_EN_CLK_ENABLE()   __HAL_RCC_GPIOA_CLK_ENABLE()
#define RS485_TX_EN()           HAL_GPIO_WritePin(RS485_EN_PORT, RS485_EN_PIN, GPIO_PIN_SET)
#define RS485_RX_EN()           HAL_GPIO_WritePin(RS485_EN_PORT, RS485_EN_PIN, GPIO_PIN_RESET)

//#define LED_PIN                  GPIO_PIN_2
//#define LED_PORT                GPIOA
//#define LED_CLK_ENABLE()        __HAL_RCC_GPIOA_CLK_ENABLE()
//#define LED_ON()                HAL_GPIO_WritePin(LED_PORT, LED_PIN, GPIO_PIN_RESET)
//#define LED_OFF()            HAL_GPIO_WritePin(LED_PORT, LED_PIN, GPIO_PIN_SET)

#define LED_PIN                 GPIO_PIN_5
#define LED_PORT                GPIOE
#define LED_CLK_ENABLE()        __HAL_RCC_GPIOE_CLK_ENABLE()
#define LED_ON()                HAL_GPIO_WritePin(LED_PORT, LED_PIN, GPIO_PIN_RESET)
#define LED_OFF()               HAL_GPIO_WritePin(LED_PORT, LED_PIN, GPIO_PIN_SET)

typedef void (*data_in)(uint8_t *data,uint16_t len , void *usrdata);

int  hal_uart_init(uint32_t baud);
void hal_uart_deinit();
int  hal_uart_send(uint8_t *data,int len ,int timeout);
void hal_uart_register_recv_data_callback(data_in func , void *usr_data);
int  hal_rs485_send(uint8_t *data,int len ,int timeout);
void hal_rs485_printf(const char *fmt, ...);
void hal_rs485_dma_rx_enable();
void hal_rs485_dma_rx_disable();

int  hal_led_init();


#endif /* HAL_H_ */
