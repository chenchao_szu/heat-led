/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-09-19     chao       the first version
 */
#ifndef APPLICATIONS_APP_LED_H_
#define APPLICATIONS_APP_LED_H_

#include <uclib/uc_def.h>

//A1 10.2
//type:A dot_width :1 index:1(0-F) data:102
typedef struct{
    uc_uint8_t  type;
    uc_uint8_t  dot_width;
    uc_int16_t  index;
    uc_int32_t data;
}led_info_t;

BEGIN_C_DECLS

uc_err_t app_led_init();
void     app_led_display(const led_info_t *info);


END_C_DECLS

#endif /* APPLICATIONS_APP_LED_H_ */
