/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-09-16     RT-Thread    first version
 */

#include <rtthread.h>
#include <stm32f1xx.h>

#define DBG_TAG "main"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

#include "app_led.h"
#include "app_net/app_net_client.h"
#include "app_net/protocol_def.h"
#include "app_btn/app_btn.h"
#include "bsp_beep.h"

#define F6_ENABLE   0

typedef enum{
    WORK_STATE_DISPLAY,
    WORK_STATE_USR,
    WORK_STATE_ADMIN
}WORK_STATE;

typedef void (*btn_click_cb)(void *ctx);

typedef struct
{
    led_info_t info;
    btn_click_cb cb_inc_click;
    btn_click_cb cb_dec_click;
    btn_click_cb cb_inc_hold_click;
    btn_click_cb cb_dec_hold_click;
    uc_uint8_t  edit_index;
}led_fun_page_t;

uc_uint8_t work_state = WORK_STATE_DISPLAY;

typedef enum {
    LED_USR_FUNS_TYPE_IC_OFFSET,             //f0
    LED_USR_FUNS_TYPE_PWR_RATED,             //f1
    LED_USR_FUNS_TYPE_LIMIT_PDM_OUT,         //f2
    LED_USR_FUNS_TYPE_PW_OVERTIME_NOW,       //f3
    LED_USR_FUNS_TYPE_PHASE_OFFSET_MAX,      //f4
    LED_USR_FUNS_TYPE_PHASE_OFFSET_MIN,      //f5
    LED_USR_FUNS_TYPE_USE_TIME_MAX,          //f6
    LED_USR_FUNS_TYPE_FREQ_MAX,              //f7
    LED_USR_FUNS_TYPE_FREQ_INIT ,            //f8
    LED_USR_FUNS_TYPE_PDM_TYPE ,          //f9 pdm or freq
}LED_USR_FUNS_TYPE;

void on_btn_click_f0_inc(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data++;
    if(f->info.data > 30) f->info.data = 30;
}

void on_btn_click_f0_dec(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data--;
    if(f->info.data < -30) f->info.data = -30;
}
void on_btn_click_f1_inc(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data ++;
}

void on_btn_click_f1_dec(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data --;
}
void on_btn_click_f2_inc(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data++;
    if(f->info.data > 100) f->info.data = 100;
}

void on_btn_click_f2_dec(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data--;
    if(f->info.data < 25)f->info.data = 25;
}
void on_btn_click_f3_inc(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data ++;
    if(f->info.data > 255)
        f->info.data = 255;
}

void on_btn_click_f3_dec(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data --;
    if(f->info.data < 0)
        f->info.data = 0;
}

void on_btn_click_f4_inc(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data ++;
    if(f->info.data > 60)
        f->info.data = 60;
}
void on_btn_click_f4_dec(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data --;
    if(f->info.data < 0)
        f->info.data = 0;
}
void on_btn_click_f5_inc(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data ++;
    if(f->info.data > 60)
        f->info.data = 60;
}

void on_btn_click_f5_dec(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data --;
    if(f->info.data < -20)
        f->info.data = -20;
}

void on_btn_click_f6_inc(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data ++;
}

void on_btn_click_f6_dec(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data --;
    if(f->info.data <= 0)
        f->info.data = 1;
}

void on_btn_click_f7_8_inc(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data ++;
    if(f->info.data > 24)
        f->info.data = 24;
}

void on_btn_click_f7_8_dec(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data --;
    if(f->info.data <= 4)
        f->info.data = 4;
}

void on_btn_click_f9_inc(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data = 1;
}

void on_btn_click_f9_dec(void *ctx)
{
    led_fun_page_t *f = (led_fun_page_t*)ctx;
    f->info.data = 0;
}

led_fun_page_t usr_funs[] =
{
    //F0 ic.offset
    {
        .info =
        {
            .type = 0x0F,
            .dot_width = 0,
            .index = 0,
            .data = 15
        },
        .cb_inc_click = on_btn_click_f0_inc,
        .cb_dec_click = on_btn_click_f0_dec,
        .cb_inc_hold_click = NULL,
        .cb_dec_hold_click = NULL,
        .edit_index = 0
    },
    //F1 pwr.rated
    {
        .info =
        {
            .type = 0x0F,
            .dot_width = 0,
            .index = 1,
            .data = 150
        },
        .cb_inc_click = on_btn_click_f1_inc,
        .cb_dec_click = on_btn_click_f1_dec,
        .cb_inc_hold_click = NULL,
        .cb_dec_hold_click = NULL,
        .edit_index = 0
    },
    //F2 pdm %
    {
        .info =
        {
            .type = 0x0F,
            .dot_width = 0,
            .index = 2,
            .data = 100
        },
        .cb_inc_click = on_btn_click_f2_inc,
        .cb_dec_click = on_btn_click_f2_dec,
        .cb_inc_hold_click = on_btn_click_f2_inc,
        .cb_dec_hold_click = on_btn_click_f2_dec,
        .edit_index = 0
    },
    //F3 pw overtime now
    {
        .info =
        {
            .type = 0x0F,
            .dot_width = 0,
            .index = 3,
            .data = 0
        },
        .cb_inc_click = on_btn_click_f3_inc,
        .cb_dec_click = on_btn_click_f3_dec,
        .cb_inc_hold_click = on_btn_click_f3_inc,
        .cb_dec_hold_click = on_btn_click_f3_dec,
        .edit_index = 0
    },
    //F4 phase offset max
    {
        .info =
        {
            .type = 0x0F,
            .dot_width = 0,
            .index = 4,
            .data = 0
        },
        .cb_inc_click = on_btn_click_f4_inc,
        .cb_dec_click = on_btn_click_f4_dec,
        .cb_inc_hold_click = on_btn_click_f4_inc,
        .cb_dec_hold_click = on_btn_click_f4_dec,
        .edit_index = 0
    },
    //F5 phase offset min
    {
        .info =
        {
            .type = 0x0F,
            .dot_width = 0,
            .index = 5,
            .data = 0
        },
        .cb_inc_click = on_btn_click_f5_inc,
        .cb_dec_click = on_btn_click_f5_dec,
        .cb_inc_hold_click = on_btn_click_f5_inc,
        .cb_dec_hold_click = on_btn_click_f5_dec,
        .edit_index = 0
    },
    //F6 use time max
    {
        .info =
        {
            .type = 0x0F,
            .dot_width = 0,
            .index = 6,
            .data = 0
        },
        .cb_inc_click = on_btn_click_f6_inc,
        .cb_dec_click = on_btn_click_f6_dec,
        .cb_inc_hold_click = on_btn_click_f6_inc,
        .cb_dec_hold_click = on_btn_click_f6_dec,
        .edit_index = 0
    },
    //F7 freq max
    {
        .info =
        {
            .type = 0x0F,
            .dot_width = 0,
            .index = 7,
            .data = 0
        },
        .cb_inc_click = on_btn_click_f7_8_inc,
        .cb_dec_click = on_btn_click_f7_8_dec,
        .cb_inc_hold_click = on_btn_click_f7_8_inc,
        .cb_dec_hold_click = on_btn_click_f7_8_dec,
        .edit_index = 0
    },
    //F8 freq init
    {
        .info =
        {
            .type = 0x0F,
            .dot_width = 0,
            .index = 8,
            .data = 0
        },
        .cb_inc_click = on_btn_click_f7_8_inc,
        .cb_dec_click = on_btn_click_f7_8_dec,
        .cb_inc_hold_click = on_btn_click_f7_8_inc,
        .cb_dec_hold_click = on_btn_click_f7_8_dec,
        .edit_index = 0
    },
    //F9 mode : pdm / adj phase
    {
        .info =
        {
            .type = 0x0F,
            .dot_width = 0,
            .index = 9,
            .data = 0
        },
        .cb_inc_click = on_btn_click_f9_inc,
        .cb_dec_click = on_btn_click_f9_dec,
        .cb_inc_hold_click = NULL,
        .cb_dec_hold_click = NULL,
        .edit_index = 0
    },

};

uc_int16_t usr_funs_index = 0;


struct ecode_info
{
    led_info_t array_ecode_led_array[32];
    uc_uint8_t num;
};

struct ecode_info ecode_info = {0};

led_info_t display_info[] ={
    //HZ F XXX
    {
        .type = 0x0F,
        .dot_width = 2,
        .index = -1,
        .data = 1150
    },
    //current A 100
    {
        .type = 0x0A,
        .dot_width = 1,
        .index = -1,
        .data = 125
    },
    //pwr out %
    {
        .type = 0x0B,
        .dot_width = 0,
        .index = -1,
        .data = 50
    }
};

led_info_t display_version ={
    .type = 0x0C,
    .dot_width = 0,
    .index = 0,
    .data = 0
};



static void _update_ecode_info();
static void _update_display_info();
static void _update_display_version();
static void _update_usr_func_info();


IWDG_HandleTypeDef hiwdg;
static struct rt_timer timer_wdg;
static void MX_IWDG_Init(void)
{
    //Tout=((4脳2^prer) 脳rlr) /40 = 64 * 625 / 40 = 1000ms
    hiwdg.Instance = IWDG;
    hiwdg.Init.Prescaler = IWDG_PRESCALER_64;
    hiwdg.Init.Reload = 625;
    HAL_IWDG_Init(&hiwdg);
}

void _timer_wdg_timeout_callback(void *ctx)
{
    HAL_IWDG_Refresh(&hiwdg);
}

#include <hal/hal.h>
int main(void)
{
    rt_uint8_t count = 0;

    uc_uint16_t usr_funs_max = UC_ARRAY_NUM(usr_funs);


//    hal_led_init();

    app_led_init();
    app_btn_init();
    app_net_client_init();
    bsp_beep_init();

    MX_IWDG_Init();
    rt_timer_init(&timer_wdg, "wdg", /* 瀹� 鏃� 鍣� 鍚� 瀛� 鏄� timer1 */
            _timer_wdg_timeout_callback, /* 瓒� 鏃� 鏃� 鍥� 璋� 鐨� 澶� 鐞� 鍑� 鏁� */
            RT_NULL, /* 瓒� 鏃� 鍑� 鏁� 鐨� 鍏� 鍙� 鍙� 鏁� */
            100, /* 瀹� 鏃� 闀� 搴︼紝 浠� OS Tick 涓� 鍗� 浣嶏紝 鍗� 10 涓� OS Tick */
            RT_TIMER_FLAG_PERIODIC); /* 鍛� 鏈� 鎬� 瀹� 鏃� 鍣� */

    rt_timer_start(&timer_wdg);

    ecode_info.num = 16;
    for(count = 0;count < 32;count ++)
    {
        ecode_info.array_ecode_led_array[count].type = 0x0E;
        ecode_info.array_ecode_led_array[count].dot_width = 0;
        ecode_info.array_ecode_led_array[count].index = -1;
        ecode_info.array_ecode_led_array[count].data = count;
    }

    struct rt_event* event_key = app_btn_key_event();

    while (1)
    {
        rt_uint32_t e = 0;
        rt_err_t ret = RT_EOK;

        ret = rt_event_recv(event_key,\
        (BTN_KEY_MODE_CLICK |\
         BTN_KEY_MODE_HOLD |\
         BTN_KEY_PAGE_CLICK |\
         BTN_KEY_EDIT_CLICK |\
         BTN_KEY_OK_CLICK |\
         BTN_KEY_INC_CLICK |\
         BTN_KEY_INC_HOLD_CLICK |\
         BTN_KEY_DEC_CLICK|\
         BTN_KEY_DEC_HOLD_CLICK),\
        RT_EVENT_FLAG_OR | RT_EVENT_FLAG_CLEAR,\
        2,&e);

//        rt_kprintf("e = %0X\r\n",e);

        switch(work_state)
        {
        case WORK_STATE_DISPLAY:
        {
//            if(e & BTN_KEY_MODE_CLICK)
//                work_state = WORK_STATE_USR;

            if(e & BTN_KEY_MODE_HOLD)
            {
                _update_usr_func_info();
                work_state = WORK_STATE_ADMIN;
                bsp_beep_make_noise(100);
            }

            if(work_state == WORK_STATE_DISPLAY)
            {
                _update_display_info();
                _update_ecode_info();
                _update_display_version();

                //display error
                for(count = 0 ;count < ecode_info.num;count++)
                {
                    led_info_t * info = &ecode_info.array_ecode_led_array[count];
                    bsp_beep_make_noise(100);
                    app_led_display(info);
                    rt_thread_mdelay(3000);
                }

                rt_bool_t run = g_net_data.signal_run;

                //display version
                if(ecode_info.num == 0 && run == RT_FALSE)
                {
                    app_led_display(&display_version);
                    rt_thread_mdelay(1000);
                }

                //display info
                if(run == RT_TRUE)
                {
                    for(count = 0;count < UC_ARRAY_NUM(display_info);count++)
                    {
                        app_led_display(display_info + count);

                        int i = 0;
                        int t = 30;
                        for(i = 0;i<t;i++)
                        {
                            _update_ecode_info();

                            if(ecode_info.num > 0)
                            {
                                break;
                            }
                            rt_thread_mdelay(100);
                        }
                        if(i < t) break;
                    }
                }
                //rt_kprintf("state display\r\n");
            }
        }
        break;

        case WORK_STATE_USR:
        {

//            rt_kprintf("state usr\r\n");
            rt_thread_mdelay(10);
            if(e & BTN_KEY_MODE_CLICK) work_state = WORK_STATE_DISPLAY;
            else if(e & BTN_KEY_PAGE_CLICK) rt_kprintf("page next ...\r\n");
            else if(e & BTN_KEY_EDIT_CLICK) rt_kprintf("edit ...\r\n");
            else if(e & BTN_KEY_OK_CLICK) rt_kprintf("ok ...\r\n");
            else if(e & BTN_KEY_INC_CLICK) rt_kprintf("inc ...\r\n");
            else if(e & BTN_KEY_DEC_CLICK) rt_kprintf("dec ...\r\n");
        }
        break;

        case WORK_STATE_ADMIN:
        {
//            rt_kprintf("state admin\r\n");
            rt_thread_mdelay(10);
            if(e & BTN_KEY_MODE_CLICK)
            {
                work_state = WORK_STATE_DISPLAY;
                bsp_beep_make_noise(20);
            }

            else if(e & BTN_KEY_PAGE_CLICK)
            {
                rt_kprintf("page next ...\r\n");

                usr_funs_index++;
#if !F6_ENABLE
                if(usr_funs_index == 6) usr_funs_index ++;
#endif
                usr_funs_index %= usr_funs_max;
                bsp_beep_make_noise(20);
            }
            else if(e & BTN_KEY_EDIT_CLICK)
            {
                rt_kprintf("edit ...\r\n");
                usr_funs_index -- ;
#if !F6_ENABLE
                if(usr_funs_index == 6) usr_funs_index --;
#endif
                if(usr_funs_index < 0 ) usr_funs_index = usr_funs_max - 1;
                bsp_beep_make_noise(20);
            }
            else if(e & BTN_KEY_OK_CLICK)
            {
                rt_kprintf("ok ...\r\n");
                bsp_beep_make_noise(1000);

                //func data update to net data
                //net data to save
                g_net_data.ic_offset_write        = usr_funs[LED_USR_FUNS_TYPE_IC_OFFSET].info.data;
                g_net_data.pwr_rated_write        = usr_funs[LED_USR_FUNS_TYPE_PWR_RATED].info.data;
                g_net_data.limit_pdm_out_write    = usr_funs[LED_USR_FUNS_TYPE_LIMIT_PDM_OUT].info.data;
                g_net_data.pw_overtime_now_write  = usr_funs[LED_USR_FUNS_TYPE_PW_OVERTIME_NOW].info.data;
                g_net_data.phase_offset_max_write = usr_funs[LED_USR_FUNS_TYPE_PHASE_OFFSET_MAX].info.data;
                g_net_data.phase_offset_min_write = usr_funs[LED_USR_FUNS_TYPE_PHASE_OFFSET_MIN].info.data;
                g_net_data.use_time_max_day_write = usr_funs[LED_USR_FUNS_TYPE_USE_TIME_MAX].info.data;
                g_net_data.freq_max_write         = usr_funs[LED_USR_FUNS_TYPE_FREQ_MAX].info.data;
                g_net_data.freq_init_write        = usr_funs[LED_USR_FUNS_TYPE_FREQ_INIT].info.data;
                g_net_data.pdm_type_write         = usr_funs[LED_USR_FUNS_TYPE_PDM_TYPE].info.data;

//                g_net_data.oc_offset_k = usr_funs[LED_USR_FUNS_TYPE_OC_OFFSET_K].info.data;
//                g_net_data.oc_offset_max = usr_funs[LED_USR_FUNS_TYPE_OC_OFFSET_MAX].info.data;
//                g_net_data.oc_timeout = usr_funs[LED_USR_FUNS_TYPE_OC_TIMEOUT].info.data;

//                if(g_net_data.ic_offset_write > g_net_data.oc_offset_max && g_net_data.oc_offset_max > 0) g_net_data.ic_offset_write = g_net_data.oc_offset_max;
//                if(g_net_data.ic_offset_write < g_net_data.oc_offset_max && g_net_data.oc_offset_max < 0) g_net_data.ic_offset_write = g_net_data.oc_offset_max;


                app_net_client_send_flash_save();
            }
            else if(e & BTN_KEY_INC_CLICK)
            {
                rt_kprintf("inc ...\r\n");
                usr_funs[usr_funs_index].cb_inc_click(&usr_funs[usr_funs_index]);
                bsp_beep_make_noise(20);
            }
            else if(e & BTN_KEY_DEC_CLICK)
            {
                rt_kprintf("dec ...\r\n");
                usr_funs[usr_funs_index].cb_dec_click(&usr_funs[usr_funs_index]);
                bsp_beep_make_noise(20);
            }
            else if(e & BTN_KEY_DEC_HOLD_CLICK)
            {
                rt_kprintf("dec hold...\r\n");
                if(usr_funs[usr_funs_index].cb_dec_hold_click)
                    usr_funs[usr_funs_index].cb_dec_hold_click(&usr_funs[usr_funs_index]);
                bsp_beep_make_noise(10);
            }
            else if(e & BTN_KEY_INC_HOLD_CLICK)
            {
                rt_kprintf("inc hold ...\r\n");
                if(usr_funs[usr_funs_index].cb_inc_hold_click)
                    usr_funs[usr_funs_index].cb_inc_hold_click(&usr_funs[usr_funs_index]);
                bsp_beep_make_noise(10);
            }

            app_led_display(&usr_funs[usr_funs_index].info);
        }
        break;

        }


    }

    return RT_EOK;
}

static void _update_ecode_info()
{
    int i = 0;

    ecode_info.num = 0;

    for(i = 0;i < 32;i ++)
    {
        if(UC_BIT_GET(g_net_data.alarm,i))
        {
            ecode_info.array_ecode_led_array[ecode_info.num].data = i;
            ecode_info.num ++;
        }

    }

//    rt_kprintf("锛侊紒锛侊紒锛侊紒锛侊紒锛侊紒锛侊紒锛侊紒锛侊紒锛侊紒alarm = 0x%X\r\n",g_net_data.alarm);
}
static void _update_display_info()
{
    float temp = g_net_data.hz * 0.001;
    display_info[0].data =  temp * 100;
    display_info[1].data =  g_net_data.current;
    display_info[2].data =  g_net_data.pwr_out_rate;

//    rt_kprintf("F = %d , A = %d , B = %d\n",\
//            display_info[0].data,\
//            display_info[1].data,\
//            display_info[2].data);

}
static void _update_display_version()
{
    struct heat_version
    {
        uc_uint16_t maj:4;
        uc_uint16_t min:4;
        uc_uint16_t mod:8;
    };
    struct heat_version *v = (struct heat_version *)&g_net_data.version;

    int dot = 1;
    int k = 10;

    if(v->mod >9 && v->mod <= 99)
    {
        dot = 2;
        k = 100;
    }
    else if(v->mod > 99 && v->mod <= 999)
    {
        dot = 3;
        k = 1000;
    }


    display_version.dot_width = dot;

    display_version.index = v->maj;
    display_version.data = v->min *k + v->mod;

//    rt_kprintf("version %d_%d.%d\n",v->maj,v->min,v->mod);

}
static void _update_usr_func_info()
{
    usr_funs[LED_USR_FUNS_TYPE_IC_OFFSET].info.data              = g_net_data.ic_offset_read;
    usr_funs[LED_USR_FUNS_TYPE_PWR_RATED].info.data              = g_net_data.pwr_rated_read;
    usr_funs[LED_USR_FUNS_TYPE_LIMIT_PDM_OUT].info.data          = g_net_data.limit_pdm_out_read;
    usr_funs[LED_USR_FUNS_TYPE_PW_OVERTIME_NOW].info.data        = g_net_data.pw_overtime_now_read;
    usr_funs[LED_USR_FUNS_TYPE_PHASE_OFFSET_MAX].info.data       = g_net_data.phase_offset_max_read;
    usr_funs[LED_USR_FUNS_TYPE_PHASE_OFFSET_MIN].info.data       = g_net_data.phase_offset_min_read;
    usr_funs[LED_USR_FUNS_TYPE_USE_TIME_MAX].info.data           = g_net_data.use_time_max_day_read;
    usr_funs[LED_USR_FUNS_TYPE_FREQ_MAX].info.data               = g_net_data.freq_max_read;
    usr_funs[LED_USR_FUNS_TYPE_FREQ_INIT].info.data              = g_net_data.freq_init_read;
    usr_funs[LED_USR_FUNS_TYPE_PDM_TYPE].info.data               = g_net_data.pdm_type_read;

//    LED_USR_FUNS_TYPE_PWR_RATED
}
