#ifndef _APP_BTN_H
#define _APP_BTN_H
#include <rtthread.h>
#include <uclib/uc_def.h>
#define BTN_KEY_MODE_CLICK      (1)
#define BTN_KEY_MODE_HOLD       (1<<1)
#define BTN_KEY_PAGE_CLICK		(1<<2)
#define BTN_KEY_EDIT_CLICK		(1<<3)
#define BTN_KEY_OK_CLICK        (1<<4)
#define BTN_KEY_INC_CLICK       (1<<5)
#define BTN_KEY_DEC_CLICK       (1<<6)
#define BTN_KEY_INC_HOLD_CLICK  (1<<7)
#define BTN_KEY_DEC_HOLD_CLICK  (1<<8)

BEGIN_C_DECLS

uc_err_t        app_btn_init(void);
struct rt_event* app_btn_key_event(void);

END_C_DECLS

#endif
