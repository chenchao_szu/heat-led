#include "app_btn.h"
#include <rtthread.h>
#include "multi_button.h"
#include <stm32f1xx.h>
#include <bsp_btn.h>


typedef struct _app_btn
{
	rt_thread_t task;
	struct Button btn_mode;
	struct Button btn_page;
	struct Button btn_edit;
	struct Button btn_ok;
	struct Button btn_inc;
	struct Button btn_dec;

	struct rt_event    event;
	
}app_btn_t;


static app_btn_t btn_obj;

static void _thread_btn_entry(void* ctx);
static rt_uint8_t _btn_mode_read();
static rt_uint8_t _btn_page_read();
static rt_uint8_t _btn_edit_read();
static rt_uint8_t _btn_inc_read();
static rt_uint8_t _btn_dec_read();
static rt_uint8_t _btn_ok_read();

static void _on_btn_mode_click(void *btn);
static void _on_btn_mode_long_hold_click(void *btn);
static void _on_btn_page_click(void *btn);
static void _on_btn_edit_click(void *btn);
static void _on_btn_inc_click(void *btn);
static void _on_btn_inc_hold_click(void *btn);
static void _on_btn_dec_click(void *btn);
static void _on_btn_dec_hold_click(void *btn);
static void _on_btn_ok_click(void *btn);



rt_err_t app_btn_init(void)
{
	rt_memset(&btn_obj,0,sizeof(app_btn_t));
	bsp_btn_init();
	
	rt_err_t result;
	
	result = rt_event_init(&btn_obj.event, "keyevent", RT_IPC_FLAG_FIFO); 
	if (result != RT_EOK) 
	{ 
		rt_kprintf("init event failed.\n"); 
		return -RT_ERROR; 
	}
	
	button_init(&btn_obj.btn_mode,_btn_mode_read,0);
	button_init(&btn_obj.btn_page,_btn_page_read,0);
	button_init(&btn_obj.btn_edit,_btn_edit_read,0);
	button_init(&btn_obj.btn_ok,  _btn_inc_read,0);
	button_init(&btn_obj.btn_inc, _btn_dec_read,0);
	button_init(&btn_obj.btn_dec, _btn_ok_read,0);

	

	
	button_attach(&btn_obj.btn_mode, SINGLE_CLICK,       _on_btn_mode_click);
	button_attach(&btn_obj.btn_mode, LONG_PRESS_HOLD,    _on_btn_mode_long_hold_click);
	button_attach(&btn_obj.btn_page, SINGLE_CLICK,       _on_btn_page_click);
	button_attach(&btn_obj.btn_edit, SINGLE_CLICK,       _on_btn_edit_click);
    button_attach(&btn_obj.btn_ok,   SINGLE_CLICK,       _on_btn_ok_click);
    button_attach(&btn_obj.btn_inc,  SINGLE_CLICK,       _on_btn_inc_click);
    button_attach(&btn_obj.btn_inc,  LONG_PRESS_HOLD,    _on_btn_inc_hold_click);
    button_attach(&btn_obj.btn_dec,  SINGLE_CLICK,       _on_btn_dec_click);
    button_attach(&btn_obj.btn_dec,  LONG_PRESS_HOLD,    _on_btn_dec_hold_click);
	

	
	btn_obj.task = rt_thread_create(SYS_THREAD_NAME_BTN,\
							_thread_btn_entry, \
							&btn_obj,\
							SYS_THREAD_STACK_SIZE_BTN,\
							SYS_THREAD_PRIORITY_BTN, \
							SYS_THREAD_TIME_SLICE_BTN);

	if (btn_obj.task != RT_NULL)
	{
			rt_thread_startup(btn_obj.task);
	}
	
	return (btn_obj.task != RT_NULL)?UC_EOK:UC_ERROR;
}

static void _thread_btn_entry(void* ctx)
{
    button_start(&btn_obj.btn_mode);
	button_start(&btn_obj.btn_page);
	button_start(&btn_obj.btn_edit);
	button_start(&btn_obj.btn_ok);
	button_start(&btn_obj.btn_inc);
	button_start(&btn_obj.btn_dec);

	while(1)
	{
		button_ticks();
		rt_thread_mdelay(5);
	}
}



static rt_uint8_t _btn_mode_read()
{
    return bsp_btn_value(BTN_MODE_GPIO_Port, BTN_MODE_Pin);
}

static rt_uint8_t _btn_page_read()
{
	return bsp_btn_value(BTN_PAGE_GPIO_Port, BTN_PAGE_Pin);
}
static rt_uint8_t _btn_edit_read()
{
	return bsp_btn_value(BTN_EDIT_GPIO_Port, BTN_EDIT_Pin);
}
static rt_uint8_t _btn_inc_read()
{
	return bsp_btn_value(BTN_INC_GPIO_Port, BTN_INC_Pin);
}
static rt_uint8_t _btn_dec_read()
{
	return bsp_btn_value(BTN_DEC_GPIO_Port, BTN_DEC_Pin);
}
static rt_uint8_t _btn_ok_read()
{
	return bsp_btn_value(BTN_OK_GPIO_Port, BTN_OK_Pin);
}
//------------------------on click callback-------------------------
static void _on_btn_mode_click(void *btn)
{
    rt_kprintf("btn mode click\r\n");
    rt_event_send(&btn_obj.event,BTN_KEY_MODE_CLICK);
    //ui_main_switch_to_group_next();
}
static void _on_btn_mode_long_hold_click(void *btn)
{
//    rt_kprintf("btn mode long hold click\r\n");
    rt_event_send(&btn_obj.event,BTN_KEY_MODE_HOLD);
}
static void _on_btn_page_click(void *btn)
{
	rt_kprintf("btn page click\r\n");
	rt_event_send(&btn_obj.event,BTN_KEY_PAGE_CLICK);
	//ui_main_switch_to_page(btn_obj.ui_page_index);
}

static void _on_btn_edit_click(void *btn)
{
	rt_kprintf("btn edit click\r\n");
	//ui_main_switch_to_group_next();
	rt_event_send(&btn_obj.event,BTN_KEY_EDIT_CLICK);
}
static void _on_btn_inc_click(void *btn)
{
    rt_kprintf("btn inc click\r\n");
    rt_event_send(&btn_obj.event,BTN_KEY_INC_CLICK);
}
static void _on_btn_inc_hold_click(void *btn)
{
//    rt_kprintf("btn inc hold click\r\n");
    rt_event_send(&btn_obj.event,BTN_KEY_INC_HOLD_CLICK);
}
static void _on_btn_dec_click(void *btn)
{
    rt_kprintf("btn dec click\r\n");
    rt_event_send(&btn_obj.event,BTN_KEY_DEC_CLICK);
}
static void _on_btn_dec_hold_click(void *btn)
{
//    rt_kprintf("btn dec hold click\r\n");
    rt_event_send(&btn_obj.event,BTN_KEY_DEC_HOLD_CLICK);
}
static void _on_btn_ok_click(void *btn)
{
    rt_kprintf("btn ok click\r\n");
    rt_event_send(&btn_obj.event,BTN_KEY_OK_CLICK);
}



struct rt_event* app_btn_key_event(void)
{
	return &btn_obj.event;
}
