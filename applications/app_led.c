/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-09-19     chao       the first version
 */
#include "app_led.h"
#include "bsp_led.h"

ALIGN(RT_ALIGN_SIZE)
static char thread_led_stack[SYS_THREAD_STACK_SIZE_LED];
static struct rt_thread thread_led;

led_info_t led_info_cur = {
        .type = 0x0A,
        .dot_width = 0,
        .index = 0,
        .data = -372
};

rt_uint8_t state = 0;
rt_uint16_t tick = 0;

static void _show_num()
{
    // head label
    bsp_led_offall();
    rt_thread_mdelay(0);

    bsp_led_show(0, led_info_cur.type, 0);
    rt_thread_mdelay(3);

    if(led_info_cur.index != -1)
    {
        bsp_led_offall();
        rt_thread_mdelay(0);

        bsp_led_show(1, led_info_cur.index, 0);
        rt_thread_mdelay(3);
    }

    if(led_info_cur.data == 0)
    {
        bsp_led_show(5, 0, 0);
        rt_thread_mdelay(3);
        return ;
    }

    // number
    rt_uint8_t s[4] = {0};

    rt_uint32_t temp = led_info_cur.data;

    if(led_info_cur.data < 0)
    {
        temp = -led_info_cur.data;
    }

    s[0] = temp / 1000;

    s[1] = temp %1000 / 100;
    s[2] = temp %100 / 10;
    s[3] = temp % 10;

    rt_uint8_t i;
    rt_uint8_t start_inx = 0;

    for(i = 0;i< 4;i++)
    {
        if(s[i] != 0 )
            break;
    }
    start_inx = i;

    //rt_kprintf("v = %d , start = %d\r\n",led_info_cur.data,start_inx);



    for(i =0 ;i<4-start_inx;i++)
    {
        bsp_led_offall();
        rt_thread_mdelay(0);

        if(led_info_cur.dot_width > 0 && (i == led_info_cur.dot_width))
            bsp_led_show(6-i - 1, s[3-i], 1);//show float number
        else
            bsp_led_show(6-i - 1, s[3-i], 0);

        rt_thread_mdelay(3);
    }
    rt_uint8_t dwidth = 4 - start_inx;
    if(led_info_cur.data < 0)
    {

        bsp_led_offall();
        rt_thread_mdelay(0);
        bsp_led_show(6-dwidth - 1, 16, 0);//-
        rt_thread_mdelay(3);
    }
}
static void _show_label()
{
//    rt_uint8_t data = 0;
//
//    bsp_led_offall();
//    rt_thread_mdelay(1);
//
//
//    switch(led_info_cur.type)
//    {
//    case LED_INFO_TYPE_DISPLAY:
//        data = 0x0A;
//        break;
//    case LED_INFO_TYPE_ERROR:
//        data = 0x0E;
//        break;
//    case LED_INFO_TYPE_PRM:
//        data = 0x0F;
//        break;
//    }
//
//    bsp_led_show(0, data, 0);
//    rt_thread_mdelay(2);
//
//    rt_uint8_t s[3] = {0};
//    s[0] = led_info_cur.index /100;
//    s[1] = led_info_cur.index %100/10;
//    s[2] = led_info_cur.index %10;
//
//    rt_uint8_t i = 0;
//    for(i = 0;i<3;i++)
//    {
//        bsp_led_offall();
//        rt_thread_mdelay(1);
//
//        bsp_led_show(i+1, s[i], 0);
//        rt_thread_mdelay(2);
//    }

}

static void _thread_app_led_entry(void *param)
{

    while(1)
    {
        _show_num();
    }

}

uc_err_t app_led_init()
{
    bsp_led_init();

    rt_thread_init(&thread_led, SYS_THREAD_NAME_LED,
            _thread_app_led_entry,
            RT_NULL, &thread_led_stack[0],
            sizeof(thread_led_stack),
            SYS_THREAD_PRIORITY_LED,
            SYS_THREAD_TIME_SLICE_LED);

    rt_thread_startup(&thread_led);

    return UC_EOK;

}
void     app_led_display(const led_info_t *info)
{
    led_info_cur = *info;
    state = 0;
    tick = 0;
}
