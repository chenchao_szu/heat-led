/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-10-01     chao       the first version
 */
#ifndef APPLICATIONS_APP_NET_CLIENT_H_
#define APPLICATIONS_APP_NET_CLIENT_H_

#include <uclib/uc_def.h>

typedef struct
{
    uc_uint16_t hz;
    uc_uint16_t current;
    uc_uint8_t  pwr_out_rate;
    uc_uint32_t alarm;
    uc_uint16_t oc_count;
    uc_uint8_t  oc_offset_k; //X10
    uc_int8_t   oc_offset_max;
    uc_uint16_t oc_timeout;
    uc_uint8_t  signal_run;
    uc_uint16_t version;
    uc_int16_t  ic_offset_read;
    uc_int16_t  ic_offset_write;
    uc_uint16_t pwr_rated_read;
    uc_uint16_t pwr_rated_write;
    uc_uint8_t  limit_pdm_out_read;
    uc_uint8_t  limit_pdm_out_write;
    uc_uint8_t  pw_overtime_now_read;
    uc_uint8_t  pw_overtime_now_write;
    uc_int8_t   phase_offset_max_read;
    uc_int8_t   phase_offset_max_write;
    uc_int8_t   phase_offset_min_read;
    uc_int8_t   phase_offset_min_write;
    uc_uint32_t use_time_max_day_read;
    uc_uint32_t use_time_max_day_write;
    uc_uint8_t  freq_max_read;
    uc_uint8_t  freq_max_write;
    uc_uint8_t  freq_init_read;
    uc_uint8_t  freq_init_write;
    uc_uint8_t  pdm_type_read;
    uc_uint8_t  pdm_type_write;
}app_net_data_t;

extern app_net_data_t g_net_data;

BEGIN_C_DECLS

uc_err_t app_net_client_init();
uc_err_t app_net_client_send_flash_save();


END_C_DECLS



#endif /* APPLICATIONS_APP_NET_CLIENT_H_ */
