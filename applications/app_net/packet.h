/*
 * Copyright (c) 20017-2021, GanZhe Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-01-04     chao       the first version
 */
#ifndef NET_PRIV_PACKET_H_
#define NET_PRIV_PACKET_H_

#include <uclib/uc_def.h>

#define PKT_HEAD         0xAA55AA55

#define SIZE_PKT_HEAD       4
#define SIZE_PKT_LEN        2
#define SIZE_PKT_SRC        1
#define SIZE_PKT_DST        1
#define SIZE_PKT_ID         5
#define SIZE_PKT_TYPE       1
#define SIZE_PKT_CMD        1
#define SIZE_PKT_CRC        1
#define SIZE_PKT_ERRCODE    1
#define SIZE_PKT_READ_NUM   1
#define SIZE_PKT_WRITE_NUM  1
#define SIZE_PKT_SYNC_NUM   1

#define TYPE_PKT_REQ           0x00
#define TYPE_PKT_REP           0x01
#define TYPE_PKT_EVENT         0x02
#define TYPE_PKT_REP_ERROR     0x81

#define CMD_PKT_PING            0x01
#define CMD_PKT_SYNC            0x02
#define CMD_PKT_DICT_READ       0x03
#define CMD_PKT_DICT_WRITE      0x04
#define CMD_PKT_BROAD_WRITE     0x05
#define CMD_PKT_BUF_WRITE       0x06
#define CMD_PKT_PLOT_REQ        0x07
#define CMD_PKT_SET_USR_RAM     0x08

#define NET_BROADCAST_ADDR      0xFF

typedef enum
{
    VALUE_TYPE_INT8 = 0,
    VALUE_TYPE_UINT8,
    VALUE_TYPE_INT16,
    VALUE_TYPE_UINT16,
    VALUE_TYPE_INT32,
    VALUE_TYPE_UINT32,
    VALUE_TYPE_INT64,
    VALUE_TYPE_UINT64,
    VALUE_TYPE_FLOAT,
    VALUE_TYPE_DOUBLE,

    VALUE_TYPE_TOTAL_SIZE
}VALUE_TYPE;

BEGIN_C_DECLS

#pragma pack(1)
typedef struct _packet_common
{
    uint32_t head;
    uint16_t len;
    uint8_t  src;
    uint8_t  dst;
    uint16_t id;
    uint8_t  type; //[bit7 = 1 :error respond]  [req:0x0,rep:0x01,event:0x02] [0x81:error rep]
    uint8_t  resv0;
    uint8_t  resv1;
    uint8_t  cmd;
    uint8_t  data[0];
}packet_common_t;

#pragma pack()



typedef union _obj_dict_index
{
    struct _obj_dict_index_bit
    {
        uint32_t parent:16;
        uint32_t child:8;
        uint32_t resv:8;
    }bit;
    uint32_t all;
}obj_dict_index_t;



typedef struct _value_any
{
    uint8_t type;
    union _u_value
    {
      uint8_t u8;
      int8_t  s8;
      uint16_t u16;
      int16_t  s16;
      uint32_t u32;
      int32_t  s32;
      uint64_t u64;
      int64_t  s64;
      float    f32;
      double   f64;
    }data;
}value_any_t;

typedef struct _obj_dict_item
{
    obj_dict_index_t index;
    value_any_t      value;
}obj_dict_item_t;

static inline uint8_t bytes_from_type(VALUE_TYPE type)
{
    uint8_t bytes = 1;
    switch(type)
    {
    case VALUE_TYPE_INT8:
    case VALUE_TYPE_UINT8:
        bytes = 1;
        break;
    case VALUE_TYPE_INT16:
    case VALUE_TYPE_UINT16:
        bytes = 2;
        break;
    case VALUE_TYPE_INT32:
    case VALUE_TYPE_UINT32:
        bytes = 4;
        break;
    case VALUE_TYPE_INT64:
    case VALUE_TYPE_UINT64:
        bytes = 8;
        break;
    case VALUE_TYPE_FLOAT:
        bytes = 4;
        break;
    case VALUE_TYPE_DOUBLE:
        bytes = 8;
        break;
    default:
        bytes = 1;
        break;
    }
    return bytes;
}


static inline uint8_t pkt_bcc_checksum(const uint8_t *buf, int wLen)
{
    uint8_t checksum = 0;
    int i;

    for(i = 0; i < wLen; i++)
        checksum ^= buf[i];

    return checksum;
}

static inline uint8_t pkt_get_ecode(const uint8_t *repFrame)
{
    return repFrame[sizeof(packet_common_t)];
}

static inline uint8_t *pkt_rep_read_data(const uint8_t *repFrame)
{
    return (uint8_t *)repFrame + sizeof(packet_common_t) + SIZE_PKT_ERRCODE + SIZE_PKT_READ_NUM + sizeof(obj_dict_index_t);
}

END_C_DECLS


#endif /* NET_PRIV_PACKET_H_ */
