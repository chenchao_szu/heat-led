/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-10-01     chao       the first version
 */
#include "app_net_client.h"
#include <sys_config.h>
#include <rtthread.h>
#include <hal/hal.h>
#include "packet.h"
#include "protocol_def.h"
#include <uclib/base/ringbuffer.h>
#include <uclib/base/ringblk_buf.h>


#define EVENT_DATA_IN           (0x01)
#define EVENT_DATA_FRAME_IN     (0x02)

#define PACKET_MAX_SIZE          128
#define RBUF_POOL_SIZE           (2*PACKET_MAX_SIZE)
#define PACKET_RECV_COPY_SIZE    PACKET_MAX_SIZE



typedef enum
{
    RECV_STATE_HEAD = 0,
    RECV_STATE_LEN,
    RECV_STATE_DATA
}RECV_STATE;


ALIGN(RT_ALIGN_SIZE)
static char thread_net_client_stack[SYS_THREAD_STACK_SIZE_NET_CLIENT];
static struct rt_thread thread_net_client;

ALIGN(RT_ALIGN_SIZE)
static char thread_net_client_parse_stack[SYS_THREAD_STACK_SIZE_NET_CLIENT_PARSE];
static struct rt_thread thread_net_client_parse;

app_net_data_t g_net_data;
uc_uint8_t     net_server_address = 0x01;
uc_uint8_t     net_own_address = 0x02;
uc_uint16_t    packet_unique_id = 0;
uc_uint8_t     net_pkt_send_buf[PACKET_MAX_SIZE];
uc_uint8_t     net_pkt_recv_buf[PACKET_MAX_SIZE];
struct rt_mutex     net_mutex_wr;



struct rt_ringbuffer rbuf;
rt_uint8_t           rbpool[RBUF_POOL_SIZE];
struct rt_event      ev_data;
struct rt_event      ev_frame;
rt_uint8_t           fsm_recv_state ;
rt_uint16_t          buf_recv_pos;
rt_rbb_t             *rbb;


static obj_dict_item_t item_ic_offset_write = {0};
static obj_dict_item_t item_pwr_rated_write = {0};
static obj_dict_item_t item_oc_offset_k = {0};
static obj_dict_item_t item_oc_offset_max = {0};
static obj_dict_item_t item_oc_timeout = {0};
static obj_dict_item_t item_ctl_flash_save_write = {0};
static obj_dict_item_t item_pw_overtime_now_write = {0};
static obj_dict_item_t item_phase_offset_max_write = {0};
static obj_dict_item_t item_phase_offset_min_write = {0};
static obj_dict_item_t item_limit_pdm_out_write = {0};
static obj_dict_item_t item_use_time_max_write = {0};
static obj_dict_item_t item_freq_max_write = {0};
static obj_dict_item_t item_freq_init_write = {0};
static obj_dict_item_t item_pdm_type_write = {0};

//read and write is in on thread

static void _show_buf(uint8_t *buf,int len)
{
    rt_kprintf("----buf data -------------");
    int i = 0;
    for(i = 0;i<len;i++)
    {
        rt_kprintf("%02X ",buf[i]);
    }
    rt_kprintf("\n");
}

static inline int _bytes_from_value_type(uc_uint8_t type)
{
    int bytes = 1;
    switch(type)
    {
    case VALUE_TYPE_INT16:
    case VALUE_TYPE_UINT16:
        bytes = 2;
        break;
    case VALUE_TYPE_INT32:
    case VALUE_TYPE_UINT32:
        bytes = 4;
        break;
    case VALUE_TYPE_INT64:
    case VALUE_TYPE_UINT64:
        bytes = 8;
        break;
    case VALUE_TYPE_FLOAT:
        bytes = 4;
        break;
    case VALUE_TYPE_DOUBLE:
        bytes = 8;
        break;
    }
    return bytes;
}

static uc_err_t _net_client_read_obj(uc_uint8_t address , obj_dict_item_t * item ,int timeout)
{
    rt_mutex_take(&net_mutex_wr, RT_WAITING_FOREVER);

    int len = sizeof(packet_common_t) + sizeof(obj_dict_index_t) + SIZE_PKT_READ_NUM + SIZE_PKT_CRC;

    packet_common_t *pkt = (packet_common_t*)net_pkt_send_buf;
    pkt->head = PKT_HEAD;
    pkt->len = len;
    pkt->src = net_own_address;
    pkt->dst = address;
    pkt->id = packet_unique_id ++;
    pkt->type = TYPE_PKT_REQ;
    pkt->cmd = CMD_PKT_DICT_READ;
    uint8_t *ptr = &pkt->data[0];
    rt_memcpy(ptr,(uint8_t*)&item->index,sizeof(obj_dict_index_t));
    ptr = ptr + sizeof(obj_dict_index_t);
    *ptr = 1;
    ptr = ptr+1;
    *ptr = pkt_bcc_checksum(net_pkt_send_buf,pkt->len - 1);

//    rt_kprintf("begin send pkt len = %d , pkt id = %d\n",len,pkt->id);

//    _show_buf(net_pkt_send_buf , len);

    int ret = hal_rs485_send(net_pkt_send_buf,len,100);
    if(ret != len)
    {
        rt_mutex_release(&net_mutex_wr);
        return UC_EIO;
    }

    rt_uint32_t e;
    if(rt_event_recv(&ev_frame, (EVENT_DATA_FRAME_IN),\
            RT_EVENT_FLAG_OR|RT_EVENT_FLAG_CLEAR,\
            timeout, &e) == RT_EOK)
    {
        while(1)
        {
            rt_rbb_blk_t blk = rt_rbb_blk_get(rbb);
            if(blk == NULL) break;
            else
            {
                packet_common_t *pkt_recv = (packet_common_t*)blk->buf;

//                rt_kprintf("pkt src id = %d , tar id = %d\n",pkt->id,pkt_recv->id);

                uint8_t err = pkt_get_ecode((const uint8_t*)blk->buf);
                if(err == ERR_OK)
                {
                    if(pkt_recv->id == pkt->id)
                    {
                        uint8_t *ptr_data = pkt_rep_read_data((const uint8_t*)blk->buf);

                        int bytes = _bytes_from_value_type(item->value.type);
                        rt_memcpy(&item->value.data.u8, ptr_data, bytes);

//                        switch(item->value.type)
//                        {
//                        case VALUE_TYPE_INT8 :
////                            item->value.data.s8 = *(int8_t *)ptr_data;
//                            rt_memcpy(&item->value.data.s8, ptr_data, 1);
//                            break;
//                        case VALUE_TYPE_UINT8 :
//                            item->value.data.u8 = *(uint8_t *)ptr_data;
//                            break;
//                        case VALUE_TYPE_INT16:
//                            item->value.data.s16 = *(int16_t *)ptr_data;
//                            break;
//                        case VALUE_TYPE_UINT16:
//                            item->value.data.u16 = *(uint16_t *)ptr_data;
//                            break;
//                        case VALUE_TYPE_INT32:
//                            item->value.data.s32 = *(int32_t *)ptr_data;
//                            break;
//                        case VALUE_TYPE_UINT32:
//                            item->value.data.u32 = *(uint32_t *)ptr_data;
//                            break;
//                        case VALUE_TYPE_INT64:
//                            item->value.data.s64 = *(int64_t *)ptr_data;
//                            break;
//                        case VALUE_TYPE_UINT64:
//                            item->value.data.u64 = *(uint64_t *)ptr_data;
//                            break;
//                        case VALUE_TYPE_FLOAT:
//                            item->value.data.f32 = *(float *)ptr_data;
//                            break;
//                        case VALUE_TYPE_DOUBLE:
//                            item->value.data.f64 = *(double *)ptr_data;
//                            break;
//                        }
                    }
                }
                rt_rbb_blk_free(rbb,blk);
            }
        }

    }
    else
    {
        rt_mutex_release(&net_mutex_wr);
        rt_kprintf("ERROR: read timeout !\r\n");
        return UC_ETIMEOUT;
    }

    rt_mutex_release(&net_mutex_wr);

    return UC_EOK;
}



static uc_err_t _net_client_write_obj(uc_uint8_t address , obj_dict_item_t * item ,int timeout)
{
    rt_mutex_take(&net_mutex_wr, RT_WAITING_FOREVER);
    int lenWrite = _bytes_from_value_type(item->value.type);

    int len = sizeof(packet_common_t) + sizeof(obj_dict_index_t) + SIZE_PKT_WRITE_NUM + lenWrite +SIZE_PKT_CRC;

    packet_common_t *pkt = (packet_common_t*)net_pkt_send_buf;
    pkt->head = PKT_HEAD;
    pkt->len = len;
    pkt->src = net_own_address;
    pkt->dst = address;
    pkt->id = packet_unique_id ++;
    pkt->type = TYPE_PKT_REQ;
    pkt->cmd = CMD_PKT_DICT_WRITE;
    uint8_t *ptr = &pkt->data[0];
    uint8_t *ptrNum = ptr + sizeof(obj_dict_index_t);
    uint8_t *ptrWrite = ptrNum + 1;
    uint8_t *ptrCRC = ptrWrite + lenWrite;
    rt_memcpy(ptr,(uint8_t*)&item->index,sizeof(obj_dict_index_t));
    *ptrNum = 1;

    rt_memcpy(ptrWrite,(const void *)&item->value.data,lenWrite);

    *ptrCRC = pkt_bcc_checksum(net_pkt_send_buf,pkt->len - 1);

//    rt_kprintf("begin send pkt len = %d , pkt id = %d\n",len,pkt->id);

    _show_buf(net_pkt_send_buf , len);

    int ret = hal_rs485_send(net_pkt_send_buf,len,100);
    if(ret != len)
    {
        rt_mutex_release(&net_mutex_wr);
        return UC_EIO;
    }

    rt_uint32_t e;
    if(rt_event_recv(&ev_frame, (EVENT_DATA_FRAME_IN),\
            RT_EVENT_FLAG_OR|RT_EVENT_FLAG_CLEAR,\
            timeout, &e) == RT_EOK)
    {

        rt_rbb_blk_t blk = rt_rbb_blk_get(rbb);
        if(blk)
        {
            packet_common_t *pkt_recv = (packet_common_t*)blk->buf;

//            rt_kprintf("pkt snd id = %d , recv id = %d\n",pkt->id,pkt_recv->id);

            uint8_t err = pkt_get_ecode((const uint8_t*)blk->buf);


            if(err != ERR_OK)
            {
                rt_rbb_blk_free(rbb,blk);
                rt_mutex_release(&net_mutex_wr);
                return UC_EIO;
            }

            if(pkt_recv->id != pkt->id)
            {
                rt_rbb_blk_free(rbb,blk);
                rt_mutex_release(&net_mutex_wr);
                return UC_EIO;
            }

            rt_rbb_blk_free(rbb,blk);
        }

    }
    else
    {
        rt_mutex_release(&net_mutex_wr);
        return UC_ETIMEOUT;
    }

    rt_mutex_release(&net_mutex_wr);

    return UC_EOK;
}

static void _dispatch_respond(uint8_t *frame , int len)
{
//TODO put frame in rbb
    rt_rbb_blk_t blk = rt_rbb_blk_alloc(rbb, len);
    if(blk == NULL)
    {
        blk = rt_rbb_blk_get(rbb);
        rt_rbb_blk_free(rbb,blk);

        blk = rt_rbb_blk_alloc(rbb, len);
    }

    rt_memcpy(blk->buf,frame,len);
    rt_rbb_blk_put(blk);

    rt_event_send(&ev_frame, EVENT_DATA_FRAME_IN);

}
void _dispatch(uint8_t *frame , int len)
{
//    int i = 0;
//    rt_kprintf("----dispatch-------------");
//    for(i = 0;i<len;i++)
//    {
//        rt_kprintf("%02X ",frame[i]);
//    }
//    rt_kprintf("\n");

    packet_common_t *pkt = (packet_common_t *)frame;
    switch(pkt->type)
    {
    case TYPE_PKT_REQ:

        break;
    case TYPE_PKT_REP:
        _dispatch_respond(frame,len);
        break;
    case TYPE_PKT_EVENT:
        break;
    case TYPE_PKT_REP_ERROR:
        _dispatch_respond(frame,len);
        break;
    }
}

void _parse_frame_FSM(uint8_t *data , int len)
{
    packet_common_t *pkt_comm = (packet_common_t *)net_pkt_recv_buf;
    int j = 0;
    for(j = 0;j<len;j++)
    {
        uint8_t v = data[j];
//        rt_kprintf("fsm recv state = %d\n",fsm_recv_state);
        switch(fsm_recv_state)
        {
        case RECV_STATE_HEAD:
        {
            pkt_comm->head <<= 8;
            pkt_comm->head |= v;
            uint32_t head = UC_BSWAP32(pkt_comm->head);
            if(head == PKT_HEAD)
            {
                pkt_comm->head = head;
                buf_recv_pos = SIZE_PKT_HEAD;
                fsm_recv_state = RECV_STATE_LEN;
            }
        }

            break;
        case RECV_STATE_LEN:
            buf_recv_pos++;
            pkt_comm->len <<= 8;
            pkt_comm->len |= v;
            if(buf_recv_pos >= SIZE_PKT_HEAD + SIZE_PKT_LEN)
            {
                pkt_comm->len = UC_BSWAP16(pkt_comm->len);
                fsm_recv_state = RECV_STATE_DATA;
                if(pkt_comm->len > PACKET_MAX_SIZE)
                {
                    rt_memset(net_pkt_recv_buf, 0, buf_recv_pos);
                    fsm_recv_state = RECV_STATE_HEAD;
                    //TODO send back len error to client
                }
            }
            break;
        case RECV_STATE_DATA:
            net_pkt_recv_buf[buf_recv_pos] = v;
            buf_recv_pos++;
            if(buf_recv_pos == SIZE_PKT_HEAD + SIZE_PKT_LEN + SIZE_PKT_SRC + SIZE_PKT_DST)
            {
                if(pkt_comm->dst != net_own_address && pkt_comm->dst != NET_BROADCAST_ADDR)
                {
                    rt_memset(net_pkt_recv_buf, 0, buf_recv_pos);
                    fsm_recv_state = RECV_STATE_HEAD;
                }
            }
            if(buf_recv_pos == pkt_comm->len)
            {
                uint8_t crc0 = net_pkt_recv_buf[buf_recv_pos - 1];
                uint8_t crc1 = pkt_bcc_checksum(net_pkt_recv_buf, buf_recv_pos - 1);
//                rt_kprintf("crc0 = %02X , crc1 = %02X\r\n",crc0,crc1);
                if(crc0 == crc1)
                {
                    _dispatch(net_pkt_recv_buf,buf_recv_pos);

                }
                else {
                    //TODO respond crc error
                }
                rt_memset(net_pkt_recv_buf, 0, buf_recv_pos);
                fsm_recv_state = RECV_STATE_HEAD;
            }
            break;
        }
    }
}

static void _thread_app_net_client_entry(void *param)
{
    uc_err_t err = UC_EOK;

    obj_dict_item_t item_hz = {0};
    obj_dict_item_t item_cur = {0};
    obj_dict_item_t item_pwr_out_rate = {0};
    obj_dict_item_t item_alarm = {0};
    obj_dict_item_t item_signal_run = {0};
    obj_dict_item_t item_version = {0};

    obj_dict_item_t item_flash_ic_offset = {0};
    obj_dict_item_t item_flash_pwr_rated = {0};
    obj_dict_item_t item_flash_pw_overtime_now = {0};
    obj_dict_item_t item_flash_limit_pdm_out = {0};
    obj_dict_item_t item_flash_phase_offset_max = {0};
    obj_dict_item_t item_flash_phase_offset_min = {0};
    obj_dict_item_t item_flash_use_time_max = {0};
    obj_dict_item_t item_flash_freq_max = {0};
    obj_dict_item_t item_flash_freq_init = {0};
    obj_dict_item_t item_flash_pdm_type = {0};


    item_hz.index.bit.parent = DICT_INDEX_RAM;
    item_hz.index.bit.child = DICT_SUBINDEX_RAM_FCUR;
    item_hz.value.type = VALUE_TYPE_UINT16;

    item_cur.index.bit.parent = DICT_INDEX_RAM;
    item_cur.index.bit.child = DICT_SUBINDEX_RAM_FBK_CUR_PHY;
    item_cur.value.type = VALUE_TYPE_FLOAT;

    item_pwr_out_rate.index.bit.parent = DICT_INDEX_RAM;
    item_pwr_out_rate.index.bit.child = DICT_SUBINDEX_RAM_PWR_OUT_RATE;
    item_pwr_out_rate.value.type = VALUE_TYPE_UINT8;

    item_alarm.index.bit.parent = DICT_INDEX_RAM;
    item_alarm.index.bit.child = DICT_SUBINDEX_RAM_ALARM;
    item_alarm.value.type = VALUE_TYPE_UINT32;

    item_signal_run.index.bit.parent = DICT_INDEX_RAM;
    item_signal_run.index.bit.child = DICT_SUBINDEX_RAM_SIGNAL_RUN;
    item_signal_run.value.type = VALUE_TYPE_UINT8;

    item_version.index.bit.parent = DICT_INDEX_RAM;
    item_version.index.bit.child = DICT_SUBINDEX_RAM_VERSION;
    item_version.value.type = VALUE_TYPE_UINT16;

    item_flash_ic_offset.index.bit.parent = DICT_INDEX_FLASH;
    item_flash_ic_offset.index.bit.child = DICT_SUBINDEX_FLASH_IC_OFFSET;
    item_flash_ic_offset.value.type = VALUE_TYPE_INT16;

    item_flash_pwr_rated.index.bit.parent = DICT_INDEX_FLASH;
    item_flash_pwr_rated.index.bit.child = DICT_SUBINDEX_FLASH_PWR_RATED;
    item_flash_pwr_rated.value.type = VALUE_TYPE_UINT16;

    item_flash_pw_overtime_now.index.bit.parent = DICT_INDEX_FLASH;
    item_flash_pw_overtime_now.index.bit.child = DICT_SUBINDEX_FLASH_PW_OVERTIME_NOW;
    item_flash_pw_overtime_now.value.type = VALUE_TYPE_UINT8;

    item_flash_limit_pdm_out.index.bit.parent = DICT_INDEX_FLASH;
    item_flash_limit_pdm_out.index.bit.child = DICT_SUBINDEX_FLASH_LIMIT_OUTPUT_PDM;
    item_flash_limit_pdm_out.value.type = VALUE_TYPE_UINT8;

    item_flash_phase_offset_max.index.bit.parent = DICT_INDEX_FLASH;
    item_flash_phase_offset_max.index.bit.child = DICT_SUBINDEX_FLASH_PHASE_OFFSET_MAX;
    item_flash_phase_offset_max.value.type = VALUE_TYPE_INT8;

    item_flash_phase_offset_min.index.bit.parent = DICT_INDEX_FLASH;
    item_flash_phase_offset_min.index.bit.child = DICT_SUBINDEX_FLASH_PHASE_OFFSET_MIN;
    item_flash_phase_offset_min.value.type = VALUE_TYPE_INT8;

    item_flash_use_time_max.index.bit.parent = DICT_INDEX_FLASH;
    item_flash_use_time_max.index.bit.child = DICT_SUBINDEX_FLASH_USETIME_MAX;
    item_flash_use_time_max.value.type = VALUE_TYPE_UINT32;

    item_flash_freq_max.index.bit.parent    = DICT_INDEX_FLASH;
    item_flash_freq_max.index.bit.child     = DICT_SUBINDEX_FLASH_FREQ_MAX;
    item_flash_freq_max.value.type          = VALUE_TYPE_UINT8;

    item_flash_freq_init.index.bit.parent   = DICT_INDEX_FLASH;
    item_flash_freq_init.index.bit.child    = DICT_SUBINDEX_FLASH_FREQ_INIT;
    item_flash_freq_init.value.type         = VALUE_TYPE_UINT8;

    item_flash_pdm_type.index.bit.parent   = DICT_INDEX_FLASH;
    item_flash_pdm_type.index.bit.child    = DICT_SUBINDEX_FLASH_PDM_TYPE;
    item_flash_pdm_type.value.type         = VALUE_TYPE_UINT8;

    int i = 0;
    int interval_read_ms = 100;

    while(1)
    {
//        rt_kprintf("hz = %d\n\n",item_hz.value.data.u16);

        if(UC_EOK ==_net_client_read_obj(net_server_address,&item_alarm ,100))
        {
            g_net_data.alarm = item_alarm.value.data.u32;
            rt_thread_mdelay(2);
        }
//        rt_kprintf("alarm = %d\n\n",item_alarm.value.data.u32);


        for(i = 0;i<20;i++)
        {
            err = _net_client_read_obj(net_server_address,&item_hz ,50);
            if(err == UC_EOK) break;
        }

//        rt_kprintf("read i = %d\r\n",i);

        if(i < 20)
        {
            g_net_data.hz = item_hz.value.data.u16;
            UC_BIT_CLEAR(g_net_data.alarm , 31);
        }
        else
        {
            UC_BIT_SET(g_net_data.alarm , 31);
            continue;
        }


//        float sum = 0;
//
//        for(i = 0;i<8;i++)
//        {
//            err = _net_client_read_obj(net_server_address,&item_cur ,100);
//            sum += item_cur.value.data.f32;
//            rt_thread_mdelay(1);
//
//        }
//
//        sum = sum / 8.0;

        _net_client_read_obj(net_server_address,&item_cur ,100);
        g_net_data.current = item_cur.value.data.f32 * 10;
        rt_thread_mdelay(interval_read_ms);


        _net_client_read_obj(net_server_address,&item_pwr_out_rate ,100);
//        rt_kprintf("pwr_out_rate = %d\n\n",item_pwr_out_rate.value.data.u8);
        g_net_data.pwr_out_rate = item_pwr_out_rate.value.data.u8;
        rt_thread_mdelay(interval_read_ms);

        _net_client_read_obj(net_server_address,&item_signal_run ,100);
        g_net_data.signal_run = item_signal_run.value.data.u8;
        rt_thread_mdelay(interval_read_ms);

        _net_client_read_obj(net_server_address,&item_version ,100);
        g_net_data.version = item_version.value.data.u16;
        rt_thread_mdelay(interval_read_ms);

        _net_client_read_obj(net_server_address,&item_flash_ic_offset ,100);
        g_net_data.ic_offset_read = item_flash_ic_offset.value.data.s16;
        rt_thread_mdelay(interval_read_ms);

        _net_client_read_obj(net_server_address,&item_flash_pwr_rated ,100);
        g_net_data.pwr_rated_read = item_flash_pwr_rated.value.data.u16;
        rt_thread_mdelay(interval_read_ms);

        _net_client_read_obj(net_server_address,&item_flash_pw_overtime_now ,100);
        g_net_data.pw_overtime_now_read = item_flash_pw_overtime_now.value.data.u8;
        rt_thread_mdelay(interval_read_ms);

        _net_client_read_obj(net_server_address,&item_flash_limit_pdm_out ,100);
        g_net_data.limit_pdm_out_read = item_flash_limit_pdm_out.value.data.u8;
        rt_thread_mdelay(interval_read_ms);

        _net_client_read_obj(net_server_address,&item_flash_phase_offset_max ,100);
        g_net_data.phase_offset_max_read = item_flash_phase_offset_max.value.data.s8;
        rt_thread_mdelay(interval_read_ms);

        _net_client_read_obj(net_server_address,&item_flash_phase_offset_min ,100);
        g_net_data.phase_offset_min_read = item_flash_phase_offset_min.value.data.s8;
        rt_thread_mdelay(interval_read_ms);

        _net_client_read_obj(net_server_address,&item_flash_use_time_max ,100);
        g_net_data.use_time_max_day_read = item_flash_use_time_max.value.data.u32;
        rt_thread_mdelay(interval_read_ms);

        _net_client_read_obj(net_server_address,&item_flash_freq_max ,100);
        g_net_data.freq_max_read = item_flash_freq_max.value.data.u8;
        rt_thread_mdelay(interval_read_ms);

        _net_client_read_obj(net_server_address,&item_flash_freq_init ,100);
        g_net_data.freq_init_read= item_flash_freq_init.value.data.u8;
        rt_thread_mdelay(interval_read_ms);

        _net_client_read_obj(net_server_address,&item_flash_pdm_type ,100);
        g_net_data.pdm_type_read= item_flash_pdm_type.value.data.u8;
        rt_thread_mdelay(interval_read_ms);

    }

}
static void _thread_app_net_client_parse_entry(void *param)
{

    while(1)
    {
        rt_uint32_t e;
        if(rt_event_recv(&ev_data, (EVENT_DATA_IN),\
                RT_EVENT_FLAG_OR|RT_EVENT_FLAG_CLEAR,\
                5000, &e) == RT_EOK)
        {
            int len = rt_ringbuffer_data_len(&rbuf);
        //    hal_rs485_printf("ringbuffer len = %d\r\n",len);
//            rt_kprintf("recv get uart data len = %d\r\n",len);

            if(len > PACKET_MAX_SIZE) len = PACKET_MAX_SIZE;
            int i = 0;
            static rt_uint8_t data[PACKET_RECV_COPY_SIZE] = {0};
            rt_base_t level;

            for(i = 0;i< len/PACKET_RECV_COPY_SIZE ;i++)
            {
                level = rt_hw_interrupt_disable();
                rt_ringbuffer_get(&rbuf, data, PACKET_RECV_COPY_SIZE);
                rt_hw_interrupt_enable(level);
//                _show_buf(data,PACKET_RECV_COPY_SIZE);
                _parse_frame_FSM(data , PACKET_RECV_COPY_SIZE);
            }
            level = rt_hw_interrupt_disable();
            rt_ringbuffer_get(&rbuf, data, len%PACKET_RECV_COPY_SIZE);
            rt_hw_interrupt_enable(level);
//            rt_kprintf("get data............\n");
//            _show_buf(data,len%PACKET_RECV_COPY_SIZE);
            _parse_frame_FSM(data , len%PACKET_RECV_COPY_SIZE);
        }
    }

}

static void _on_data_in_callback(uint8_t *data,uint16_t len , void *usrdata)
{
    if(rt_ringbuffer_space_len(&rbuf) >= len)
    {
        rt_ringbuffer_put(&rbuf, data, len);
        rt_event_send(&ev_data, EVENT_DATA_IN);
    }
}


uc_err_t app_net_client_init()
{
    //TODO init from cfg
    net_server_address = 0x01;
    net_own_address = 0x02;

    fsm_recv_state = 0;
    buf_recv_pos = 0;

    item_ic_offset_write.index.bit.parent = DICT_INDEX_FLASH;
    item_ic_offset_write.index.bit.child = DICT_SUBINDEX_FLASH_IC_OFFSET;
    item_ic_offset_write.value.type = VALUE_TYPE_INT16;

    item_pwr_rated_write.index.bit.parent = DICT_INDEX_FLASH;
    item_pwr_rated_write.index.bit.child = DICT_SUBINDEX_FLASH_PWR_RATED;
    item_pwr_rated_write.value.type = VALUE_TYPE_UINT16;

    item_oc_offset_k.index.bit.parent = DICT_INDEX_FLASH;
    item_oc_offset_k.index.bit.child = DICT_SUBINDEX_FLASH_OC_OFFSET_K;
    item_oc_offset_k.value.type = VALUE_TYPE_UINT8;

    item_oc_offset_max.index.bit.parent = DICT_INDEX_FLASH;
    item_oc_offset_max.index.bit.child = DICT_SUBINDEX_FLASH_OC_OFFSET_MAX;
    item_oc_offset_max.value.type = VALUE_TYPE_INT8;

    item_oc_timeout.index.bit.parent = DICT_INDEX_FLASH;
    item_oc_timeout.index.bit.child = DICT_SUBINDEX_FLASH_OC_TIMEOUT;
    item_oc_timeout.value.type = VALUE_TYPE_UINT16;

    item_limit_pdm_out_write.index.bit.parent = DICT_INDEX_FLASH;
    item_limit_pdm_out_write.index.bit.child = DICT_SUBINDEX_FLASH_LIMIT_OUTPUT_PDM;
    item_limit_pdm_out_write.value.type = VALUE_TYPE_UINT8;

    item_pw_overtime_now_write.index.bit.parent = DICT_INDEX_FLASH;
    item_pw_overtime_now_write.index.bit.child = DICT_SUBINDEX_FLASH_PW_OVERTIME_NOW;
    item_pw_overtime_now_write.value.type = VALUE_TYPE_UINT8;

    item_phase_offset_max_write.index.bit.parent = DICT_INDEX_FLASH;
    item_phase_offset_max_write.index.bit.child = DICT_SUBINDEX_FLASH_PHASE_OFFSET_MAX;
    item_phase_offset_max_write.value.type = VALUE_TYPE_INT8;

    item_phase_offset_min_write.index.bit.parent = DICT_INDEX_FLASH;
    item_phase_offset_min_write.index.bit.child = DICT_SUBINDEX_FLASH_PHASE_OFFSET_MIN;
    item_phase_offset_min_write.value.type = VALUE_TYPE_INT8;

    item_use_time_max_write.index.bit.parent = DICT_INDEX_FLASH;
    item_use_time_max_write.index.bit.child = DICT_SUBINDEX_FLASH_USETIME_MAX;
    item_use_time_max_write.value.type = VALUE_TYPE_UINT32;

    item_freq_max_write.index.bit.parent = DICT_INDEX_FLASH;
    item_freq_max_write.index.bit.child  = DICT_SUBINDEX_FLASH_FREQ_MAX;
    item_freq_max_write.value.type       = VALUE_TYPE_UINT8;

    item_freq_init_write.index.bit.parent = DICT_INDEX_FLASH;
    item_freq_init_write.index.bit.child  = DICT_SUBINDEX_FLASH_FREQ_INIT;
    item_freq_init_write.value.type       = VALUE_TYPE_UINT8;

    item_pdm_type_write.index.bit.parent = DICT_INDEX_FLASH;
    item_pdm_type_write.index.bit.child = DICT_SUBINDEX_FLASH_PDM_TYPE;
    item_pdm_type_write.value.type = VALUE_TYPE_UINT8;

    item_ctl_flash_save_write.index.bit.parent = DICT_INDEX_CONTROL;
    item_ctl_flash_save_write.index.bit.child = DICT_SUBINDEX_CONTROL_FLASH_SAVE;
    item_ctl_flash_save_write.value.type = VALUE_TYPE_UINT8;




    rt_mutex_init (&net_mutex_wr, "w-r", RT_IPC_FLAG_PRIO);

    rbb =  rt_rbb_create(PACKET_MAX_SIZE, 3);

    rt_ringbuffer_init(&rbuf, rbpool, RT_ALIGN_DOWN(RBUF_POOL_SIZE,4));

    rt_event_init(&ev_data, "DataIn", RT_IPC_FLAG_FIFO);

    rt_event_init(&ev_frame, "frame", RT_IPC_FLAG_FIFO);


    rt_thread_init(&thread_net_client, SYS_THREAD_NAME_NET_CLIENT,
            _thread_app_net_client_entry,
            RT_NULL, &thread_net_client_stack[0],
            sizeof(thread_net_client_stack),
            SYS_THREAD_PRIORITY_NET_CLIENT,
            SYS_THREAD_TIME_SLICE_NET_CLIENT);

    rt_thread_startup(&thread_net_client);

    rt_thread_init(&thread_net_client_parse, SYS_THREAD_NAME_NET_CLIENT_PARSE,
            _thread_app_net_client_parse_entry,
            RT_NULL, &thread_net_client_parse_stack[0],
            sizeof(thread_net_client_parse_stack),
            SYS_THREAD_PRIORITY_NET_CLIENT_PARSE,
            SYS_THREAD_TIME_SLICE_NET_CLIENT_PARSE);

    rt_thread_startup(&thread_net_client_parse);

    hal_uart_init(115200);
//    hal_uart_init(115200);
    hal_uart_register_recv_data_callback(_on_data_in_callback , NULL);

    return UC_EOK;
}
uc_err_t app_net_client_send_flash_save()
{
    item_ic_offset_write.value.data.s16 = g_net_data.ic_offset_write;
    _net_client_write_obj(0x01, &item_ic_offset_write, 1000);

    item_pwr_rated_write.value.data.u16 = g_net_data.pwr_rated_write;
    _net_client_write_obj(0x01, &item_pwr_rated_write, 1000);

    item_limit_pdm_out_write.value.data.u8 = g_net_data.limit_pdm_out_write;
    _net_client_write_obj(0x01, &item_limit_pdm_out_write, 1000);

    item_pw_overtime_now_write.value.data.u8 = g_net_data.pw_overtime_now_write;
    _net_client_write_obj(0x01, &item_pw_overtime_now_write, 1000);

    item_phase_offset_max_write.value.data.s8 = g_net_data.phase_offset_max_write;
    _net_client_write_obj(0x01, &item_phase_offset_max_write, 1000);

    item_phase_offset_min_write.value.data.s8 = g_net_data.phase_offset_min_write;
    _net_client_write_obj(0x01, &item_phase_offset_min_write, 1000);

    item_use_time_max_write.value.data.u32 = g_net_data.use_time_max_day_write;
    _net_client_write_obj(0x01, &item_use_time_max_write, 1000);

    item_freq_max_write.value.data.u8 = g_net_data.freq_max_write;
    _net_client_write_obj(0x01, &item_freq_max_write, 1000);

    item_freq_init_write.value.data.u8 = g_net_data.freq_init_write;
    _net_client_write_obj(0x01, &item_freq_init_write, 1000);

    item_pdm_type_write.value.data.u8 = g_net_data.pdm_type_write;
    _net_client_write_obj(0x01, &item_pdm_type_write, 1000);

    item_ctl_flash_save_write.value.data.u8 = 1;
    _net_client_write_obj(0x01, &item_ctl_flash_save_write, 1000);

    return UC_EOK;
}
//uc_err_t app_net_client_read_HZ(uc_uint16_t *v)
//{
//    return UC_EOK;
//}
//uc_err_t app_net_client_read_current(uc_uint16_t *v)
//{
//    return UC_EOK;
//}
//uc_err_t app_net_client_read_pwr_out_rate(uc_uint8_t *v)
//{
//    return UC_EOK;
//}
//uc_err_t app_net_client_read_alarm(uc_uint32_t *v)
//{
//    return UC_EOK;
//}
//uc_err_t app_net_client_read_oc_count(uc_uint16_t *v)
//{
//    return UC_EOK;
//}
//uc_err_t app_net_client_read_prm_ic_offset(uc_int16_t *v)
//{
//    return UC_EOK;
//}
//uc_err_t app_net_client_read_prm_oc_offset_k(uc_uint8_t *v)
//{
//    return UC_EOK;
//}
//uc_err_t app_net_client_read_prm_oc_offset_max(uc_int8_t *v)
//{
//    return UC_EOK;
//}
//uc_err_t app_net_client_read_prm_oc_timeout(uc_uint16_t *v)
//{
//    return UC_EOK;
//}
