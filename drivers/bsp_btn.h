/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-09-21     chao       the first version
 */
#ifndef DRIVERS_BSP_BTN_H_
#define DRIVERS_BSP_BTN_H_

#include <uclib/uc_def.h>
#include <stm32f1xx.h>

#define BTN_EDIT_Pin        GPIO_PIN_13
#define BTN_EDIT_GPIO_Port  GPIOC
#define BTN_OK_Pin          GPIO_PIN_14
#define BTN_OK_GPIO_Port    GPIOB
#define BTN_INC_Pin         GPIO_PIN_13
#define BTN_INC_GPIO_Port   GPIOB
#define BTN_DEC_Pin         GPIO_PIN_15
#define BTN_DEC_GPIO_Port   GPIOB
#define BTN_MODE_Pin        GPIO_PIN_8
#define BTN_MODE_GPIO_Port  GPIOA
#define BTN_PAGE_Pin        GPIO_PIN_9
#define BTN_PAGE_GPIO_Port  GPIOA


BEGIN_C_DECLS

uc_err_t   bsp_btn_init();
uc_uint8_t bsp_btn_value(GPIO_TypeDef *port , uc_uint32_t pin);

END_C_DECLS

#endif /* DRIVERS_BSP_BTN_H_ */
