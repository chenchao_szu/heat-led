/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-10-01     chao       the first version
 */
#ifndef DRIVERS_BSP_BEEP_H_
#define DRIVERS_BSP_BEEP_H_

#include <uclib/uc_def.h>

BEGIN_C_DECLS

uc_err_t   bsp_beep_init();
void       bsp_beep_make_noise(uc_uint16_t timems);

END_C_DECLS


#endif /* DRIVERS_BSP_BEEP_H_ */
