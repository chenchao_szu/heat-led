/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-09-19     chao       the first version
 */
#ifndef DRIVERS_BSP_LED_H_
#define DRIVERS_BSP_LED_H_

#include <uclib/uc_def.h>

BEGIN_C_DECLS

uc_err_t bsp_led_init();
void     bsp_led_show(uc_uint8_t index , uc_int8_t data , uc_bool_t dot_en);
void     bsp_led_offall();

END_C_DECLS

#endif /* DRIVERS_BSP_LED_H_ */
