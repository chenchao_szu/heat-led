/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-09-19     chao       the first version
 */
#include "bsp_led.h"
#include <stm32f1xx.h>

//共阴数码管段选表，无小数点：
unsigned char const distab_1[17] = {0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d, 0x07,0x7f,0x6f,0x77,0x7c,0x39,0x5e,0x79,0x71,0x40};

//共阴数码管段选表，有小数点：
unsigned char const distab_2[17] = {0xbf,0x86,0xdb,0xcf,0xe6,0xed,0xfd, 0x87,0xff,0xef,0xf7,0xfc,0xb9,0xde,0xf9,0xf1,0x41};


#define SEG_C_Pin           GPIO_PIN_1
#define SEG_C_GPIO_Port     GPIOB
#define SEG_B_Pin           GPIO_PIN_2
#define SEG_B_GPIO_Port     GPIOB
#define SEG_A_Pin           GPIO_PIN_12
#define SEG_A_GPIO_Port     GPIOB

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void _led_MX_GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                            |GPIO_PIN_4|GPIO_PIN_6|GPIO_PIN_7, GPIO_PIN_RESET);

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0|SEG_C_Pin|SEG_B_Pin|SEG_A_Pin, GPIO_PIN_RESET);

    /*Configure GPIO pins : PA0 PA1 PA2 PA3
                             PA4 PA6 PA7 */
    GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                            |GPIO_PIN_4|GPIO_PIN_6|GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /*Configure GPIO pin : PA5 */
    GPIO_InitStruct.Pin = GPIO_PIN_5;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /*Configure GPIO pin : PB0 */
    GPIO_InitStruct.Pin = GPIO_PIN_0;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /*Configure GPIO pins : SEG_C_Pin SEG_B_Pin SEG_A_Pin */
    GPIO_InitStruct.Pin = SEG_C_Pin|SEG_B_Pin|SEG_A_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, 0);//enable OE
}


uc_err_t bsp_led_init()
{
    _led_MX_GPIO_Init();
    return UC_EOK;
}
void     bsp_led_show(uc_uint8_t index , uc_int8_t data , uc_bool_t dot_en)
{
    uc_uint8_t code = 0;

    if(data != -1)
    {
        code = distab_1[data % 17];
        if(dot_en) code = distab_2[data % 17];
    }
    //seg on
    HAL_GPIO_WritePin(SEG_A_GPIO_Port, SEG_A_Pin, (index & 0x01));
    HAL_GPIO_WritePin(SEG_B_GPIO_Port, SEG_B_Pin, (index & 0x02));
    HAL_GPIO_WritePin(SEG_C_GPIO_Port, SEG_C_Pin, (index & 0x04));


    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, (code & 0x80));
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, (code & 0x40));
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, (code & 0x20));
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, (code & 0x10));
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, (code & 0x08));
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, (code & 0x04));
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, (code & 0x02));
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, (code & 0x01));

//    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, 1);
//    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, 1);
//    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, 1);
//    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, 1);
//    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, 1);
//    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 1);
//    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 1);
//    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 1);


}
void     bsp_led_offall()
{
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, 0);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, 0);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, 0);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, 0);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, 0);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 0);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);
}
