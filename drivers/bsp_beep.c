/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-10-01     chao       the first version
 */
#include "bsp_beep.h"
#include <stm32f1xx.h>
#include <rtthread.h>
static struct rt_timer timer_beep;

static void _beep_gpio_init()
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    __HAL_RCC_GPIOC_CLK_ENABLE();


    /*Configure GPIO pin : BTN_EDIT_Pin */
    GPIO_InitStruct.Pin = GPIO_PIN_14;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    //HAL_GPIO_WritePin(GPIOC, GPIO_PIN_14, GPIO_PIN_SET);

}
static void _beep_on(int en)
{
    if(en)
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_14, GPIO_PIN_SET);
    else {
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_14, GPIO_PIN_RESET);
    }
}
void _beep_timeout_callback(void *ctx)
{
    _beep_on(0);

    rt_kprintf("timeout..\n");
}
uc_err_t   bsp_beep_init()
{
    _beep_gpio_init();

    rt_timer_init(&timer_beep, "tim_beep", /* 定 时 器 名 字 是 timer1 */
            _beep_timeout_callback, /* 超 时 时 回 调 的 处 理 函 数 */
            RT_NULL, /* 超 时 函 数 的 入 口 参 数 */
            100, /* 定 时 长 度， 以 OS Tick 为 单 位， 即 10 个 OS Tick */
            RT_TIMER_FLAG_ONE_SHOT); /* 周 期 性 定 时 器 */



    return UC_EOK;
}
void       bsp_beep_make_noise(uc_uint16_t timems)
{
    rt_tick_t tick = timems;

    rt_timer_control(&timer_beep, RT_TIMER_CTRL_SET_TIME, (void*) &tick);

    _beep_on(1);

    rt_timer_start(&timer_beep);

    rt_kprintf("start timer \n");
}
