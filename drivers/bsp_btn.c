/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-09-21     chao       the first version
 */
#include "bsp_btn.h"



/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void btn_MX_GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    /*Configure GPIO pin : BTN_EDIT_Pin */
    GPIO_InitStruct.Pin = BTN_EDIT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(BTN_EDIT_GPIO_Port, &GPIO_InitStruct);

    /*Configure GPIO pins : BTN_OK_Pin BTN_INC_Pin BTN_DEC_Pin */
    GPIO_InitStruct.Pin = BTN_OK_Pin|BTN_INC_Pin|BTN_DEC_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /*Configure GPIO pins : BTN_MODE_Pin BTN_PAGE_Pin */
    GPIO_InitStruct.Pin = BTN_MODE_Pin|BTN_PAGE_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

uc_err_t bsp_btn_init()
{
    btn_MX_GPIO_Init();
    return UC_EOK;
}
uc_uint8_t bsp_btn_value(GPIO_TypeDef *port , uc_uint32_t pin)
{
    return HAL_GPIO_ReadPin(port, pin);
}
