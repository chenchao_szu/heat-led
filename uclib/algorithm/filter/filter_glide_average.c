#include "filter_glide_average.h"
#include <string.h>
uc_err_t filter_glid_average_init(filter_glid_average_t *self, uc_uint8_t deep)
{
	self->prm.n = deep; //TO BE ADDED
	if (self->prm.n >= FILTER_GLID_AVERAGE_MAX_SIZE)
		return UC_ERANGE;

	memset(self->data, 0, sizeof(self->data));

	return UC_EOK;
}
void  filter_glid_average_do(filter_glid_average_t *self, uc_uint16_t *ad_new, uc_uint16_t *out)
{
	self->data[self->prm.n] = *ad_new;
	uc_uint8_t i = 0;
	uc_uint32_t sum = 0;
	for (; i < self->prm.n; i++)
	{
		self->data[i] = self->data[i+1];
		sum += self->data[i];
	}
	*out = sum / self->prm.n;
}
