#ifndef _FILTER_GLIDE_AVERAGE_H
#define _FILTER_GLIDE_AVERAGE_H
#include "../../uc_def.h"


#define FILTER_GLID_AVERAGE_MAX_SIZE (32 + 1)

struct _filter_glid_average_prm
{
	uc_uint8_t n;
};

typedef struct _filter_glid_average
{
	struct _filter_glid_average_prm prm;
	uc_uint16_t data[FILTER_GLID_AVERAGE_MAX_SIZE];
}filter_glid_average_t;

uc_err_t filter_glid_average_init(filter_glid_average_t *self, uc_uint8_t deep);
void  filter_glid_average_do(filter_glid_average_t *self, uc_uint16_t *ad_new, uc_uint16_t *out);
#endif // !_FILTER_GLIDE_AVERAGE_H
