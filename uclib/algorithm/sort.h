/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-03-26     chao       the first version
 */
#ifndef UCLIB_ALGORITHM_SORT_H_
#define UCLIB_ALGORITHM_SORT_H_

#include <../uc_def.h>

BEGIN_C_DECLS

//typedef int (*data_cmp_func)(void *ctx , void *data);

//static inline int cmp_inc_int16(void *a , void *b)
//{
//    return (uc_int16_t)a - (uc_int16_t)b;
//}

//static inline uc_err_t bubble_sort (void **array, uc_size_t nr , data_cmp_func cmp)
//{
//    uc_size_t i = 0;
//    uc_size_t max = 0;
//    uc_size_t right = 0;
//    return_value_if_fail(array != NULL && cmp != NULL ,UC_EPARAMS);
//
//    if(nr < 2)
//    {
//        return UC_EOK;
//    }
//
//    for(right = nr - 1; right > 0; right--)
//    {
//        for(i = 1, max = 0; i < right; i++)
//        {
//            if(cmp(array[i], array[max]) > 0)
//            {
//                max = i;
//            }
//        }
//
//        if(cmp(array[max], array[right]) > 0)
//        {
//            void* data = array[right];
//            array[right] = array[max];
//            array[max] = data;
//        }
//    }
//    return UC_EOK;
//}

#define bubble_sort(type,array,nr,cmp) \
{\
    uc_size_t i = 0;\
    uc_size_t max = 0;\
    uc_size_t right = 0;\
    for(right = nr - 1; right > 0; right--)\
    {\
        for(i = 1, max = 0; i < right; i++)\
        {\
            if(cmp(array[i], array[max]) > 0)\
            {\
                max = i;\
            }\
        }\
        if(cmp(array[max], array[right]) > 0)\
        {\
            type data = array[right];\
            array[right] = array[max];\
            array[max] = data;\
        }\
    }\
}

END_C_DECLS

#endif /* UCLIB_ALGORITHM_SORT_H_ */
