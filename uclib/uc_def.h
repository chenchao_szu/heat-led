/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-03-26     chao       the first version
 */
#ifndef UCLIB_UC_DEF_H_
#define UCLIB_UC_DEF_H_

#include <stdlib.h>
#include <stdio.h>
#include <sys_config.h>

#ifdef __cplusplus
extern "C" {
#endif



/* UC basic data type definitions */
#ifndef UC_USING_ARCH_DATA_TYPE
typedef signed   char                   uc_int8_t;      /**<  8bit integer type */
typedef signed   short                  uc_int16_t;     /**< 16bit integer type */
typedef signed   int                    uc_int32_t;     /**< 32bit integer type */
typedef unsigned char                   uc_uint8_t;     /**<  8bit unsigned integer type */
typedef unsigned short                  uc_uint16_t;    /**< 16bit unsigned integer type */
typedef unsigned int                    uc_uint32_t;    /**< 32bit unsigned integer type */

#ifdef ARCH_CPU_64BIT
typedef signed long                     uc_int64_t;     /**< 64bit integer type */
typedef unsigned long                   uc_uint64_t;    /**< 64bit unsigned integer type */
#else
typedef signed long long                uc_int64_t;     /**< 64bit integer type */
typedef unsigned long long              uc_uint64_t;    /**< 64bit unsigned integer type */
#endif
#endif

typedef int                             uc_bool_t;      /**< boolean type */
typedef long                            uc_base_t;      /**< Nbit CPU related date type */
typedef unsigned long                   uc_ubase_t;     /**< Nbit unsigned CPU related data type */

typedef uc_base_t                       uc_err_t;       /**< Type for error number */
typedef uc_uint32_t                     uc_time_t;      /**< Type for time stamp */
typedef uc_uint32_t                     uc_tick_t;      /**< Type for tick count */
typedef uc_base_t                       uc_flag_t;      /**< Type for flags */
typedef uc_base_t                       uc_size_t;      /**< Type for size number */
typedef uc_ubase_t                      uc_dev_t;       /**< Type for device */
typedef uc_base_t                       uc_off_t;       /**< Type for offset */

/* boolean type definitions */
#define UC_TRUE                         1               /**< boolean true  */
#define UC_FALSE                        0               /**< boolean fails */

/**@}*/

/* maximum value of base type */
#define UC_UINT8_MAX                    0xff            /**< Maxium number of UINT8 */
#define UC_UINT16_MAX                   0xffff          /**< Maxium number of UINT16 */
#define UC_UINT32_MAX                   0xffffffff      /**< Maxium number of UINT32 */
#define UC_TICK_MAX                     UC_UINT32_MAX   /**< Maxium number of tick */


/**
 * @addtogroup Error
 */

/**@{*/

/* UC error code definitions */
#define UC_EOK                          0               /**< There is no error */
#define UC_ERROR                        (-1)               /**< A generic error happens */
#define UC_ETIMEOUT                     (-2)               /**< Timed out */
#define UC_EFULL                        (-3)              /**< The resource is full */
#define UC_EEMPTY                       (-4)              /**< The resource is empty */
#define UC_ENOMEM                       (-5)              /**< No memory */
#define UC_ENOSYS                       (-6)              /**< No system */
#define UC_EBUSY                        (-7)              /**< Busy */
#define UC_EIO                          (-8)               /**< IO error */
#define UC_EINTR                        (-9)              /**< Interrupted system call */
#define UC_EINVAL                       (-10)             /**< Invalid argument */
#define UC_EPARAMS                      (-11)             /**< Invalid argument */
#define UC_ERANGE                       (-12)
#define UC_ENOTFIND                     (-13)
#define UC_ELENGTH                      (-14)
#define UC_ETCP_RECV                    (-15)
#define UC_ETCP_WRITE                   (-16)


#define UC_ALIGN_SIZE           4

/**@}*/

/**
 * @ingroup BasicDef
 *
 * @def UC_ALIGN(size, align)
 * Return the most contiguous size aligned at specified width. UC_ALIGN(13, 4)
 * would return 16.
 */
#define UC_ALIGN(size, align)           (((size) + (align) - 1) & ~((align) - 1))

/**
 * @ingroup BasicDef
 *
 * @def UC_ALIGN_DOWN(size, align)
 * Return the down number of aligned at specified width. UC_ALIGN_DOWN(13, 4)
 * would return 12.
 */
#define UC_ALIGN_DOWN(size, align)      ((size) & ~((align) - 1))

/**
 * @ingroup BasicDef
 *
 * @def UC_NULL
 * Similar as the \c NULL in C library.
 */
#define UC_NULL                         (0)

#define  uc_inline static __inline

#ifdef __cplusplus
#define BEGIN_C_DECLS extern "C" {
#define END_C_DECLS }
#else
#define BEGIN_C_DECLS
#define END_C_DECLS
#endif

//TODO
#if defined(LINUX)
#include <assert.h>
#define UC_ASSERT  assert

#elif defined(FREERTOS)
#include <FreeRTOS.h>
#define UC_ASSERT  configASSERT

#elif defined(RTTHREAD)
#include <rtthread.h>
#include <rthw.h>

#define delay_ms(x) rt_thread_mdelay(x)
#define delay_us(x) rt_hw_us_delay(x)

#else

#endif

#define UC_DEBUG 1

#if(UC_DEBUG == 1)
//TODO
#if (USE_ULOG == 1)
#define UC_LOG_D LOG_D
#define UC_LOG_I LOG_I
#define UC_LOG_W LOG_W
#define UC_LOG_E LOG_E
#define UC_LOG_A LOG_A
#else
#define UC_LOG_D  rt_kprintf
#define UC_LOG_I  rt_kprintf
#define UC_LOG_W  rt_kprintf
#define UC_LOG_E  rt_kprintf
#define UC_LOG_A  rt_kprintf
#endif

#else

#if (USE_ULOG == 1)
#define UC_LOG_D LOG_D
#define UC_LOG_I LOG_I
#define UC_LOG_W LOG_W
#define UC_LOG_E LOG_E
#define UC_LOG_A LOG_A
#else
#define UC_LOG_E
#define UC_LOG_D
#define UC_LOG_I
#define UC_LOG_W
#define UC_LOG_E
#define UC_LOG_A
#endif

#endif

#define return_if_fail(p) \
  if (!(p)) {             \
    return;               \
  }

#define return_value_if_fail(p, value)                  \
  if (!(p)) {                                           \
    UC_LOG_E("%s:%d " #p "\n", __FUNCTION__, __LINE__); \
    return (value);                                     \
  }

//判断x是否是2的次方
#define UC_IS_POW_OF_2(x) ((x) != 0 && (((x) & ((x) - 1)) == 0))
//取a和b中最小值
#define UC_MIN(a, b) (((a) < (b)) ? (a) : (b))
#define UC_MAX(a, b) (((a) > (b)) ? (a) : (b))

#define UC_ARRAY_NUM(x) (sizeof(x)/sizeof(x[0]))

#define UC_UNUSED(x) (void)x;

#define UC_BSWAP16(x) \
         ((uc_uint16_t)( \
                      (((uc_uint16_t)(x) & (uc_uint16_t)0x00ff) << 8 ) | \
                      (((uc_uint16_t)(x) & (uc_uint16_t)0xff00) >> 8 ) ))
#define UC_BSWAP32(x) \
      ((((uc_uint32_t)(x) & 0xff000000) >> 24) | \
       (((uc_uint32_t)(x) & 0x00ff0000) >> 8) | \
       (((uc_uint32_t)(x) & 0x0000ff00) << 8) | \
       (((uc_uint32_t)(x) & 0x000000ff) << 24))

#define UC_BSWAP64(x) \
      ((((uc_uint64_t)(x) & 0xff00000000000000) >> 56) | \
       (((uc_uint64_t)(x) & 0x00ff000000000000) >> 40) | \
       (((uc_uint64_t)(x) & 0x0000ff0000000000) >> 24) | \
       (((uc_uint64_t)(x) & 0x000000ff00000000) >> 8) | \
       (((uc_uint64_t)(x) & 0x00000000ff000000) << 8) | \
       (((uc_uint64_t)(x) & 0x0000000000ff0000) << 24) | \
       (((uc_uint64_t)(x) & 0x000000000000ff00) << 40) | \
       (((uc_uint64_t)(x) & 0x00000000000000ff) << 56))


#define UC_BIT_GET(dat,i) ((dat&((uc_uint64_t)1<<i))?1:0)
#define UC_BIT_SET(dat,i) ((dat)|=((uc_uint64_t)1<<(i)))
#define UC_BIT_CLEAR(dat,i) ((dat)&=(~((uc_uint64_t)0x01<<(i))))


#define ENDIAN_LITTLE                       0
#define ENDIAN_BIG                          1

#ifdef __cplusplus
}
#endif

#endif /* UCLIB_UC_DEF_H_ */
